# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 11:49:13 2023

@author: ricca
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
from scipy.stats import maxwell
from scipy.interpolate import CubicSpline

from matplotlib.colors import LogNorm
from matplotlib.ticker import FormatStrFormatter

from matplotlib.legend_handler import HandlerLine2D
import matplotlib.lines
class SymHandler(HandlerLine2D):
    def create_artists(self, legend, orig_handle,xdescent, ydescent, width, height, fontsize, trans):
        xx= 0.6*height
        return super(SymHandler, self).create_artists(legend, orig_handle,xdescent, xx, width, height, fontsize, trans)

plt.rcParams.update({'font.size':35})

my_alpha=2
my_alpha+=1

my_Z=10
my_Z+=1

directory = 'E:/SEVN_work_dir/SEVN_outputs/sevn_output_alpha'

CE_parameter = np.array([0.7, 2, 4], float)
metallicity = np.array([0.0002, 0.0004, 0.0006, 0.0008, 0.001, 
                        0.002, 0.004, 0.006, 0.008, 0.01, 
                        0.017], float)

X = []

for alpha in range(my_alpha):
    for Z in range(my_Z):
        for counter in range(8):
            file = directory+str(alpha)+'_Z'+str(Z)+'/master_'+str(counter)+'.txt'
            regex_str = 'CONAKED;.*?;.*?:.*?:.*?:.*?:.*?:(.*?):'
            with open(file, 'r') as fo:
                temp = re.findall(regex_str, fo.read())
            X = X+temp

X = np.array(X, float)

step = 0.1

plt.title(fr'PDF of CO-naked cores mass - Step of {step} $M_\odot$')

h = plt.hist(X, bins=np.arange(1, 7+step, step), color='white', edgecolor='black',
              lw=2, density=True)

plt.xlabel(r'Mass [$M_\odot$]')
plt.ylabel('PDF')

plt.grid(axis='y')

plt.savefig('plots/CO_distribution.png', dpi=200, bbox_inches='tight')

plt.show()

##############################################################################

a_i, a_f = [], []

for alpha in range(my_alpha):
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        for i in range(len(df)):
            a_i.append(df['a_i'][i])
            a_f.append(df['a_f'][i])

k = 0
for i in range(len(a_i)):
    if a_i[i]<a_f[i]:
        k+=1
print(round(k*100/len(a_i),2), round((len(a_i)-k)*100/len(a_i),2))

##############################################################################

primary, secondary = [], []
x = np.genfromtxt('input.in')

for i in range(len(x)):
    M = []
    M.append(x[i][0])
    M.append(x[i][5])
    primary.append(max(M))
    secondary.append(min(M))
M = primary+secondary

step = 0.5

plt.title(fr'Cumulative distribution of input mass - step of {step} $M_\odot$')

h = plt.hist(M, bins=np.arange(0, ((max(M)//step)+1)*step,step),
              color='black', histtype='step', cumulative=True, 
              density=True)

plt.xlim(0, 150)

plt.xlabel(r'Mass [$M_\odot$]')

plt.grid()

plt.tight_layout()

plt.savefig('plots/Cumulative_input_file.png', dpi=200, bbox_inches='tight')

plt.show()

##

step = 0.25

plt.title('Mass distribution of input primary and secondary stars''\n'fr'step of {step} $M_\odot$')

plt.hist(primary, bins=np.arange(((min(primary)//step)+1)*step, ((max(primary)//step)+1)*step,step),
              color='blue', histtype='step', 
              density=True, label='Primary')

plt.hist(secondary, bins=np.arange(((min(secondary)//step)+1)*step, ((max(secondary)//step)+1)*step,step),
              color='red', histtype='step', 
              density=True, label='Secondary')

plt.xticks(np.arange(0,22.5,2.5), ['0.0','2.5','5.0','7.5','10.0','12.5','15.0','17.5','20.0'], 
            rotation=60)

plt.xlim(0, 20)

plt.xlabel(r'Mass [$M_\odot$]')
plt.ylabel('PDF')

plt.legend(loc='upper right')
plt.grid()

plt.tight_layout()

plt.savefig('plots/Primary_secondary_distribution.png', dpi=200, bbox_inches='tight')

plt.show()

## 

xmin, xmax, ymin, ymax = 2,150,2,150
resolution = 1

fig, ax = plt.subplots()

# ax.set_title(fr'Histogram of initial primary VS initial secondary mass - Grid resolution: {resolution}$M_\odot \times${resolution}$M_\odot$')

h = ax.hist2d(primary, secondary,
              bins=[int((xmax-xmin)/resolution),int((ymax-ymin)/resolution)],
              range=[[xmin, xmax], [ymin, ymax]], norm=LogNorm(), cmap='viridis')
fig.colorbar(h[3], ax=ax, label='Counts')

ax.set_xlabel(r'Mass of primary [$M_\odot$]')
ax.set_ylabel(r'Mass of secondary [$M_\odot$]')

ax.set_xlim(0, 150)
ax.set_ylim(0, 150)

ax.grid()

plt.tight_layout()

plt.savefig('plots/hist2d_input_file.png', dpi=200, bbox_inches='tight')

plt.show()

#############################################################################

ECSN = []
CCSN = []

for alpha in range(my_alpha):
    temp1, temp2 = [], []
    for Z in range(my_Z):
        temp = np.genfromtxt(directory+str(alpha)+'_Z'+str(Z)+'/data_recap.txt')
        temp1.append(int(temp[0][1])), temp2.append(int(temp[1][1]))
    ECSN.append(temp1)
    CCSN.append(temp2)

ECSN = np.array(ECSN, int)
CCSN = np.array(CCSN, int)
total = ECSN+CCSN

my_lw = 5

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)

fig.set_size_inches(20,12)

ax1.set_xscale('log')

y = CCSN/total

ax1.plot(metallicity, y[0], c='black', lw=my_lw, ls='solid', label=r'$\alpha_{CE}=0.7$')
ax1.plot(metallicity, y[1], c='black', lw=my_lw, ls='dashed', label=r'$\alpha_{CE}=2.0$')
ax1.plot(metallicity, y[2], c='black', lw=my_lw, ls='dashdot', label=r'$\alpha_{CE}=4.0$')

# ax1.text(1.6e-3,0.9,r'$\uparrow$CCSN$\uparrow$', ha='center', va='bottom', fontsize=35)
ax1.set_ylabel(r'$f_{CCSN}$')

y = ECSN/total

ax2.plot(metallicity, y[0], c='black', lw=my_lw, ls='solid')
ax2.plot(metallicity, y[1], c='black', lw=my_lw, ls='dashed')
ax2.plot(metallicity, y[2], c='black', lw=my_lw, ls='dashdot')

# ax2.text(1.6e-3,0.1,r'$\downarrow$ECSN$\downarrow$', ha='center', va='top', fontsize=35)
ax2.set_ylabel(r'$f_{ECSN}$')

ax1.set_ylim(0.87, 1)
ax2.set_ylim(0, 0.13)

ax1.spines.bottom.set_visible(False)
ax2.spines.top.set_visible(False)
ax1.xaxis.tick_top()
# ax1.tick_params(labeltop=False)  # don't put tick labels at the top
ax2.xaxis.tick_bottom()

d = 0.5  # proportion of vertical to horizontal extent of the slanted line
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=50,
              linestyle="none", color='k', mec='k', mew=1, clip_on=False)
ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)

ax1.set_xlim(metallicity[0], metallicity[-1])
ax2.set_xlim(metallicity[0], metallicity[-1])

ax2.set_xlabel(r'Metallicity $Z$')

fig.legend(loc=(0.25,0.5), ncol=3)

ax1.grid()
ax2.grid()

plt.tight_layout()

fig.subplots_adjust(hspace=0.05)

plt.savefig('plots/ECSN_CCSN_fraction.png', dpi=200, bbox_inches='tight')

plt.show()

#############################################################################

x = np.linspace(0,1500, 10000)

my_maxwellian = maxwell.pdf(x, loc=0, scale=265)

NK = [[],[],[]]
NK_broken = [[],[],[]]

for alpha in range(my_alpha):
    for Z in range(my_Z):
        for counter in range(8):
            temp2 = np.genfromtxt(directory+str(alpha)+'_Z'+str(Z)+'/broken_NK.txt')
            for i in range(len(temp2)):
                NK_broken[alpha].append(temp2[i])
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        temp0 = df['NK_0']
        temp1 = df['NK_1']
        
        for i in range(len(df)):
            NK[alpha].append(temp0[i])
            NK[alpha].append(temp1[i])
    print(f'>>> {alpha+1}/{my_alpha} <<<')

step = 50

fig, (ax0,ax1,ax2) = plt.subplots(1,3, sharex=True, sharey=True)

fig.set_size_inches(30,10)

ax0.set_title(fr'$\alpha_{{CE}}$={CE_parameter[0]}')
h0=ax0.hist(NK[0], bins=np.arange(0,1500+step,step),
              rwidth=1, color='white', edgecolor='black', density=True, lw=2)
hb0=ax0.hist(NK_broken[0], bins=np.arange(0,1500+step,step), 
              rwidth=0.35, color='silver', edgecolor='black', density=True)
ax0.plot(x,my_maxwellian,c='black',ls='dotted', lw=5)

ax0.set_xticks([0,250,500,750,1000,1250], ['0','250','500','750','1000','1250'], 
                rotation=30)

ax0.set_xlim(0,1500)
ax0.grid()

ax1.set_title(fr'$\alpha_{{CE}}$={CE_parameter[1]}')
h1=ax1.hist(NK[1], bins=np.arange(0,1500+step,step),
              rwidth=1, color='white', edgecolor='black', density=True, lw=2)
hb1=ax1.hist(NK_broken[1], bins=np.arange(0,1500+step,step), 
              rwidth=0.35, color='silver', edgecolor='black', density=True)
ax1.plot(x,my_maxwellian,c='black',ls='dotted', lw=5)

ax1.set_xticks([0,250,500,750,1000,1250], ['0','250','500','750','1000','1250'], 
                rotation=30)

ax1.grid()

ax1.set_xlabel('Modulus of natal kick [km/s]')

ax2.set_title(fr'$\alpha_{{CE}}$={CE_parameter[2]}')
h2=ax2.hist(NK[2], bins=np.arange(0,1500+step,step),
              rwidth=1, color='white', edgecolor='black', density=True, 
              label='Intact BNSS', lw=2)
hb2=ax2.hist(NK_broken[2], bins=np.arange(0,1500+step,step), 
              rwidth=0.35, color='silver', edgecolor='black', density=True, 
              label='Broken after NS')
ax2.plot(x,my_maxwellian,c='black',ls='dotted', lw=5, label=r'Maxwellian $\sigma=265$km/s')

ax2.set_xticks([0,250,500,750,1000,1250], ['0','250','500','750','1000','1250'], 
                rotation=30)

ax2.legend(loc=(-1.8,-0.35), ncol=3)
ax2.grid()

# plt.tight_layout()

fig.subplots_adjust(wspace=0)

plt.savefig('plots/NK_distribution.png', dpi=200, bbox_inches='tight')

plt.show()

###

x = np.arange(25, max(h0[1]),50)
xs = np.linspace(0,1500, 10000)

cs0 = CubicSpline(x, h0[0])
cs1 = CubicSpline(x, h1[0])
cs2 = CubicSpline(x, h2[0])

csb0 = CubicSpline(x, hb0[0])
csb1 = CubicSpline(x, hb1[0])
csb2 = CubicSpline(x, hb2[0])

fig, (ax0,ax1,ax2) = plt.subplots(1,3, sharex=True, sharey=True)

fig.set_size_inches(30,10)

ax0.set_title(fr'$\alpha_{{CE}}$={CE_parameter[0]}')
ax1.set_title(fr'$\alpha_{{CE}}$={CE_parameter[1]}')
ax2.set_title(fr'$\alpha_{{CE}}$={CE_parameter[2]}')

ax0.plot(xs, cs0(xs), c='black', lw=5, label='BNSS')
ax1.plot(xs, cs1(xs), c='black', lw=5)
ax2.plot(xs, cs2(xs), c='black', lw=5)

ax0.plot(xs, csb0(xs), c='black', lw=5, ls='dashed', label='Broken after NS')
ax1.plot(xs, csb1(xs), c='black', lw=5, ls='dashed')
ax2.plot(xs, csb2(xs), c='black', lw=5, ls='dashed')

my_maxwellian = maxwell.pdf(xs, loc=0, scale=265)

ax0.plot(xs, my_maxwellian, lw=5, c='black', ls='dotted', label=r'Maxwellian $\sigma=265$ km/s')
ax1.plot(xs, my_maxwellian, lw=5, c='black', ls='dotted')
ax2.plot(xs, my_maxwellian, lw=5, c='black', ls='dotted')

ax0.set_xlim(0,1250)
ax0.set_ylim(0,None)
ax0.grid()
ax1.grid()
ax2.grid()

ax0.set_xticks([0,250,500,750,1000], ['0','250','500','750','1000'], 
                rotation=30)
ax1.set_xticks([0,250,500,750,1000], ['0','250','500','750','1000'], 
                rotation=30)
ax2.set_xticks([0,250,500,750,1000], ['0','250','500','750','1000'], 
                rotation=30)

ax1.set_xlabel('Modulus of natal kick [km/s]')
ax0.set_ylabel('PDF')

ax0.legend(loc=(0.25,-0.35), ncol=3)

fig.subplots_adjust(wspace=0)

plt.savefig('plots/NK_distribution_lines.png', dpi=200, bbox_inches='tight')

plt.show()

#############################################################################

CHA_l = [[],[],[]]
CHB_l = [[],[],[]]

CHA1_l = [[],[],[]]
CHA2_l = [[],[],[]]
CHA3_l = [[],[],[]]
CHA4_1_l = [[],[],[]]
CHA4_2_l = [[],[],[]]
CHA5_1_l = [[],[],[]]
CHA5_2_l = [[],[],[]]
CHA6_1_l = [[],[],[]]
CHA6_2_l = [[],[],[]]

CHB1_l = [[],[],[]]
CHB2_l = [[],[],[]]
CHB3_l = [[],[],[]]
CHB4_l = [[],[],[]]
CHB5_l = [[],[],[]]

CHC_l = [[],[],[]]

CHD_l = [[],[],[]]

CHE_l = [[],[],[]]

OTH_l = [[],[],[]]
TOT_l = [[],[],[]]

for alpha in range(my_alpha):
    # test = 'plots/channels/Channels_alpha'+str(alpha)+'.png'
    # if exists(test)==True:
    #     print(f'>>> Plot Channels_alpha{alpha} exists already <<<')
    # if exists(test)==False:
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        CHA = 0
        CHB = 0
        
        CHA1 = 0
        CHA2 = 0
        CHA3 = 0
        CHA4_1 = 0
        CHA4_2 = 0
        CHA5_1 = 0
        CHA5_2 = 0
        CHA6_1 = 0
        CHA6_2 = 0
        
        CHB1 = 0
        CHB2 = 0
        CHB3 = 0
        CHB4 = 0
        CHB5 = 0
        
        CHC = 0
        
        CHD = 0
        
        CHE = 0
        
        OTH = 0
        
        
        for i in range(len(df)):
            t_RLO, t_CE, t_HE_0, t_HE_1, t_NS_0, t_NS_1 = df['t_RLO'][i],df['t_CE'][i],df['t_HE_0'][i],df['t_HE_1'][i],df['t_NS_0'][i],df['t_NS_1'][i]
                            
            # Canali di formazione
            if (t_RLO!=0. and
                t_CE!=0. and
                t_HE_0!=0. and
                t_HE_1!=0. and
                t_RLO<=t_HE_0 and
                t_HE_0<=t_HE_1 and
                t_HE_1<t_CE and
                t_CE<=t_NS_0):
                CHA1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_0):
                CHA1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<t_NS_1):
                CHA3+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_1+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_2+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_1+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_1+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_2+=1
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<=t_CE and
                  t_CE<=t_NS_1):
                CHB2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_1):
                CHB3+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_1):
                CHB4+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_NS_1):
                CHB5+=1
            
            elif (t_CE!=0. and
                  t_RLO!=0. and
                  t_CE<t_RLO and
                  t_RLO<=t_NS_0):
                CHC+=1
            elif (t_CE==0. and
                  t_RLO!=0. and
                  t_RLO<t_NS_0):
                CHC+=1
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHD+=1
            
            elif (t_RLO!=0. and
                  t_CE==0. and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHE+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHE+=1
            
            else:
                OTH+=1
                # print(df['ID'][i])
            
        TOT_l[alpha].append(len(df)-OTH)
        
        CHA_l[alpha].append(CHA1+CHA2+CHA3+CHA4_1+CHA4_2+CHA5_1+CHA5_2+CHA6_1+CHA6_2)
        CHB_l[alpha].append(CHB1+CHB2+CHB3+CHB4+CHB5)
        
        CHA1_l[alpha].append(CHA1)
        CHA2_l[alpha].append(CHA2)
        CHA3_l[alpha].append(CHA3)
        CHA4_1_l[alpha].append(CHA4_1)
        CHA4_2_l[alpha].append(CHA4_2)
        CHA5_1_l[alpha].append(CHA5_1)
        CHA5_2_l[alpha].append(CHA5_2)
        CHA6_1_l[alpha].append(CHA6_1)
        CHA6_2_l[alpha].append(CHA6_2)
        
        CHB1_l[alpha].append(CHB1)
        CHB2_l[alpha].append(CHB2)
        CHB3_l[alpha].append(CHB3)
        CHB4_l[alpha].append(CHB4)
        CHB5_l[alpha].append(CHB5)
        
        CHC_l[alpha].append(CHC)
        
        CHD_l[alpha].append(CHD)
        
        CHE_l[alpha].append(CHE)
        
        OTH_l[alpha].append(OTH)

fig, ax = plt.subplots(1,3, sharex=True, sharey=True)

fig.set_size_inches(30,10)

for i in range(len(ax)):
    my_ax = ax[i]
    
    channels = np.array([CHA1_l[i], CHA2_l[i], CHA3_l[i], CHA4_1_l[i], CHA4_2_l[i], CHA5_1_l[i], CHA5_2_l[i], CHA6_1_l[i], 
                CHA6_2_l[i], CHB1_l[i], CHB2_l[i], CHB3_l[i], CHB4_l[i], CHB5_l[i], CHC_l[i], CHD_l[i], CHE_l[i]], int)
    
    names = np.array(['Channel','A1', 'A2', 'A3', 'A4-1', 'A4-2', 'A5-1', 'A5-2', 'A6-1', 
                'A6-2', 'B1', 'B2', 'B3', 'B4', 'B5', 'C', 'D', 'E'], str)
    
    TOT = np.array(TOT_l[i])
    
    CHA = np.array(CHA_l[i], int)
    CHB = np.array(CHB_l[i], int)
    
    y = [np.zeros(my_Z,int)]
    for j in range(1,len(channels)):
        y.append(sum(channels[1:j+1]))
    
    my_color = plt.cm.tab20(np.linspace(0,1,20))
    my_color = np.concatenate((my_color[0:2], my_color[3:14],my_color[14:15],my_color[2:3],
                                my_color[15:]))
    
    my_ax.set_title(fr'$\alpha_{{CE}}$={CE_parameter[i]}')
    
    my_ax.set_xscale('log')
    
    my_ax.plot(metallicity, y[0]/TOT, c='black', lw=1)
    my_ax.fill_between(metallicity, np.zeros(my_Z,int)/TOT, y[0]/TOT, color=my_color[0])
    
    for j in range(1, len(y)):
        my_ax.plot(metallicity, y[j]/TOT, c='black', lw=1)
        
        if j==1:
            A2_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==3:
            A4_1_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==4:
            A4_2_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==5:
            A5_1_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==6:
            A5_2_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==10:
            B2_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==13:
            B5_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==14:
            C_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==15:
            D_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        elif j==16:
            E_ch = my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
        else:
            my_ax.fill_between(metallicity, y[j-1]/TOT, y[j]/TOT, color=my_color[j])
    
    my_ax.plot(metallicity, CHA/TOT, c='black', lw=5)
    my_ax.plot(metallicity, (CHA+CHB)/TOT, c='black', lw=5)
    
    temp_A = CHA/TOT
    temp_B = (CHA+CHB)/TOT
    dx = 1e-3
    
    dy = temp_A[5]-temp_A[4]
    
    rotn = np.degrees(np.arctan2(dy, dx))
    dx = 2*dx/5+1e-3
    dy = dy/2+min(temp_A[4], temp_A[5])
    
    my_ax.text(dx,dy-0.01,r'Ch.A$\downarrow$', ha='center', va='top', fontsize=25,
        rotation=rotn, rotation_mode='anchor', transform_rotates_text=True)
    my_ax.text(dx,dy+0.01,r'Ch.B$\uparrow$', ha='center', va='bottom', fontsize=25,
        rotation=rotn, rotation_mode='anchor', transform_rotates_text=True)
    
    dy = temp_B[5]-temp_B[4]
    
    rotn = np.degrees(np.arctan2(dy, dx))
    dy = dy/2+min(temp_B[4], temp_B[5])
    
    my_ax.text(dx,dy-0.01,r'Ch.B$\downarrow$', ha='center', va='top', fontsize=25,
        rotation=rotn, rotation_mode='anchor', transform_rotates_text=True)
    
    my_ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    my_ax.yaxis.set_ticks(np.linspace(0, 1, 6))
    my_ax.set_xlim(metallicity[0], metallicity[-1])
    my_ax.set_ylim(0, 1)
    
    if i==1:
        my_ax.set_xlabel(r'Metallicity $Z$')
        leg = my_ax.legend([A2_ch,A4_1_ch,A4_2_ch,A5_1_ch,A5_2_ch,
                          B2_ch,B5_ch,
                          C_ch,D_ch,E_ch], 
                        ['Channel A2','Channel A4-1','Channel A4-2','Channel A5-1','Channel A5-2',
                          'Channel B2','Channel B5',
                          'Channel C','Channel D','Channel E',], edgecolor='black', 
                        title='More relevant channels:',
                        framealpha=1, handler_map={matplotlib.lines.Line2D: SymHandler()}, 
                        handleheight=1, labelspacing=0.5, ncol=5, loc=(-1.075,-0.45),
                        fontsize=30)
        leg._legend_box.align = "left"
    my_ax.grid(visible=True, which='major', axis='both', color='black', alpha=0.8, ls='dotted')
    my_ax.grid(visible=True, which='minor', axis='both', color='black', alpha=0.8, ls='dotted')
    
    if i==0:
        my_ax.set_ylabel(r'$f_{BNSS}$')
    
# plt.tight_layout()

fig.subplots_adjust(wspace=0)

plt.savefig('plots/Channels.png', dpi=200, bbox_inches='tight')

plt.show()

#############################################################################

CHA_l = [[],[],[]]
CHB_l = [[],[],[]]

CHA1_l = [[],[],[]]
CHA2_l = [[],[],[]]
CHA3_l = [[],[],[]]
CHA4_1_l = [[],[],[]]
CHA4_2_l = [[],[],[]]
CHA5_1_l = [[],[],[]]
CHA5_2_l = [[],[],[]]
CHA6_1_l = [[],[],[]]
CHA6_2_l = [[],[],[]]

CHB1_l = [[],[],[]]
CHB2_l = [[],[],[]]
CHB3_l = [[],[],[]]
CHB4_l = [[],[],[]]
CHB5_l = [[],[],[]]

CHC_l = [[],[],[]]

CHD_l = [[],[],[]]

CHE_l = [[],[],[]]

OTH_l = [[],[],[]]

for alpha in range(my_alpha):
    # test = 'plots/channels/Channels_alpha'+str(alpha)+'.png'
    # if exists(test)==True:
    #     print(f'>>> Plot Channels_alpha{alpha} exists already <<<')
    # if exists(test)==False:
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        CHA = []
        CHB = []
        
        CHA1 = []
        CHA2 = []
        CHA3 = []
        CHA4_1 = []
        CHA4_2 = []
        CHA5_1 = []
        CHA5_2 = []
        CHA6_1 = []
        CHA6_2 = []
        
        CHB1 = []
        CHB2 = []
        CHB3 = []
        CHB4 = []
        CHB5 = []
        
        CHC = []
        
        CHD = []
        
        CHE = []
        
        OTH = []
        
        
        for i in range(len(df)):
            t_RLO, t_CE, t_HE_0, t_HE_1, t_NS_0, t_NS_1 = df['t_RLO'][i],df['t_CE'][i],df['t_HE_0'][i],df['t_HE_1'][i],df['t_NS_0'][i],df['t_NS_1'][i]
                            
            # Canali di formazione
            if (t_RLO!=0. and
                t_CE!=0. and
                t_HE_0!=0. and
                t_HE_1!=0. and
                t_RLO<=t_HE_0 and
                t_HE_0<=t_HE_1 and
                t_HE_1<t_CE and
                t_CE<=t_NS_0):
                CHA1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_0):
                CHA1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA2.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<t_NS_1):
                CHA3.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_2.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_2.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_2.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_2.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<=t_CE and
                  t_CE<=t_NS_1):
                CHB2.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_1):
                CHB3.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_1):
                CHB4.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_NS_1):
                CHB5.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_CE!=0. and
                  t_RLO!=0. and
                  t_CE<t_RLO and
                  t_RLO<=t_NS_0):
                CHC.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_CE==0. and
                  t_RLO!=0. and
                  t_RLO<t_NS_0):
                CHC.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHD.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_RLO!=0. and
                  t_CE==0. and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHE.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHE.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
            
            else:
                OTH.append([max(df['M0_i'][i], df['M1_i'][i]), min(df['M0_i'][i], df['M1_i'][i]),
                              max(df['M0_f'][i], df['M1_f'][i]), min(df['M0_f'][i], df['M1_f'][i]),
                              df['a_i'][i], df['a_f'][i], df['t_CO_0'][i], df['t_CO_1'][i]])
                # print(df['ID'][i])
        
        CHA_l[alpha].append(CHA1+CHA2+CHA3+CHA4_1+CHA4_2+CHA5_1+CHA5_2+CHA6_1+CHA6_2)
        CHB_l[alpha].append(CHB1+CHB2+CHB3+CHB4+CHB5)
        
        CHA1_l[alpha].append(CHA1)
        CHA2_l[alpha].append(CHA2)
        CHA3_l[alpha].append(CHA3)
        CHA4_1_l[alpha].append(CHA4_1)
        CHA4_2_l[alpha].append(CHA4_2)
        CHA5_1_l[alpha].append(CHA5_1)
        CHA5_2_l[alpha].append(CHA5_2)
        CHA6_1_l[alpha].append(CHA6_1)
        CHA6_2_l[alpha].append(CHA6_2)
        
        CHB1_l[alpha].append(CHB1)
        CHB2_l[alpha].append(CHB2)
        CHB3_l[alpha].append(CHB3)
        CHB4_l[alpha].append(CHB4)
        CHB5_l[alpha].append(CHB5)
        
        CHC_l[alpha].append(CHC)
        
        CHD_l[alpha].append(CHD)
        
        CHE_l[alpha].append(CHE)
        
        OTH_l[alpha].append(OTH)
        
        # Nota: i dati in queste liste sono formattati così:
        # M_i primaria, M_i secondaria, M_f primaria, M_f secondaria, a_i, a_f
        
        # 17 canali di formazione

ratios = [[],[],[]]
my_channel = [CHA2_l, CHA4_2_l, CHA5_2_l, CHB2_l]

for alpha in range(my_alpha):
    for channel in range(len(my_channel)):
        ch = my_channel[channel][alpha]
        counter = 0
        total = 0
        for Z in range(my_Z):
            for i in range(len(ch[Z])):
                a,b = ch[Z][i][6],ch[Z][i][7]
                if a!=0:
                    counter+=1
                if b!=0:
                    counter+=1
            total+=len(ch[Z])*2
        ratios[alpha].append(round(counter*100/total,2))

totals = ['20.72','31.94','31.90','37.31']

plt.rcParams.update({'font.size':20})

fig, ax = plt.subplots(4,3,sharex=True)

for alpha in range(my_alpha):
    Mi_prim_A2 = []
    Mi_prim_A4_2 = []
    Mi_prim_A5_2 = []
    Mi_prim_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mi_prim_A2.append(CHA2_l[alpha][Z][system][0])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mi_prim_A4_2.append(CHA4_2_l[alpha][Z][system][0])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mi_prim_A5_2.append(CHA5_2_l[alpha][Z][system][0])
        for system in range(len(CHB2_l[alpha][Z])):
            Mi_prim_B2.append(CHB2_l[alpha][Z][system][0])
    
    Mi_prim_A2 = np.array(Mi_prim_A2, float)
    Mi_prim_A4_2 = np.array(Mi_prim_A4_2, float)
    Mi_prim_A5_2 = np.array(Mi_prim_A5_2, float)
    Mi_prim_B2 = np.array(Mi_prim_B2, float)
    
    # print(min(Mi_prim_A2), max(Mi_prim_A2), len(Mi_prim_A2))
    # print(min(Mi_prim_A4_2), max(Mi_prim_A4_2), len(Mi_prim_A4_2))
    # print(min(Mi_prim_A5_2), max(Mi_prim_A5_2), len(Mi_prim_A5_2))
    # print(min(Mi_prim_B2), max(Mi_prim_B2), len(Mi_prim_B2))
    # print('='*70)
    
    Mi_sec_A2 = []
    Mi_sec_A4_2 = []
    Mi_sec_A5_2 = []
    Mi_sec_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mi_sec_A2.append(CHA2_l[alpha][Z][system][1])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mi_sec_A4_2.append(CHA4_2_l[alpha][Z][system][1])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mi_sec_A5_2.append(CHA5_2_l[alpha][Z][system][1])
        for system in range(len(CHB2_l[alpha][Z])):
            Mi_sec_B2.append(CHB2_l[alpha][Z][system][1])
    
    Mi_sec_A2 = np.array(Mi_sec_A2, float)
    Mi_sec_A4_2 = np.array(Mi_sec_A4_2, float)
    Mi_sec_A5_2 = np.array(Mi_sec_A5_2, float)
    Mi_sec_B2 = np.array(Mi_sec_B2, float)
    
    # print(min(Mi_sec_A2), max(Mi_sec_A2), len(Mi_sec_A2))
    # print(min(Mi_sec_A4_2), max(Mi_sec_A4_2), len(Mi_sec_A4_2))
    # print(min(Mi_sec_A5_2), max(Mi_sec_A5_2), len(Mi_sec_A5_2))
    # print(min(Mi_sec_B2), max(Mi_sec_B2), len(Mi_sec_B2))
    # print('='*70)
    
    Mf_prim_A2 = []
    Mf_prim_A4_2 = []
    Mf_prim_A5_2 = []
    Mf_prim_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mf_prim_A2.append(CHA2_l[alpha][Z][system][2])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mf_prim_A4_2.append(CHA4_2_l[alpha][Z][system][2])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mf_prim_A5_2.append(CHA5_2_l[alpha][Z][system][2])
        for system in range(len(CHB2_l[alpha][Z])):
            Mf_prim_B2.append(CHB2_l[alpha][Z][system][2])
    
    Mf_prim_A2 = np.array(Mf_prim_A2, float)
    Mf_prim_A4_2 = np.array(Mf_prim_A4_2, float)
    Mf_prim_A5_2 = np.array(Mf_prim_A5_2, float)
    Mf_prim_B2 = np.array(Mf_prim_B2, float)
    
    # print(min(Mf_prim_A2), max(Mf_prim_A2), len(Mf_prim_A2))
    # print(min(Mf_prim_A4_2), max(Mf_prim_A4_2), len(Mf_prim_A4_2))
    # print(min(Mf_prim_A5_2), max(Mf_prim_A5_2), len(Mf_prim_A5_2))
    # print(min(Mf_prim_B2), max(Mf_prim_B2), len(Mf_prim_B2))
    # print('='*70)
    
    Mf_sec_A2 = []
    Mf_sec_A4_2 = []
    Mf_sec_A5_2 = []
    Mf_sec_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mf_sec_A2.append(CHA2_l[alpha][Z][system][3])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mf_sec_A4_2.append(CHA4_2_l[alpha][Z][system][3])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mf_sec_A5_2.append(CHA5_2_l[alpha][Z][system][3])
        for system in range(len(CHB2_l[alpha][Z])):
            Mf_sec_B2.append(CHB2_l[alpha][Z][system][3])
    
    Mf_sec_A2 = np.array(Mf_sec_A2, float)
    Mf_sec_A4_2 = np.array(Mf_sec_A4_2, float)
    Mf_sec_A5_2 = np.array(Mf_sec_A5_2, float)
    Mf_sec_B2 = np.array(Mf_sec_B2, float)
    
    # print(min(Mf_sec_A2), max(Mf_sec_A2), len(Mf_sec_A2))
    # print(min(Mf_sec_A4_2), max(Mf_sec_A4_2), len(Mf_sec_A4_2))
    # print(min(Mf_sec_A5_2), max(Mf_sec_A5_2), len(Mf_sec_A5_2))
    # print(min(Mf_sec_B2), max(Mf_sec_B2), len(Mf_sec_B2))
    # print('='*70)
    
    my_bins = 26
    my_lw = 1
    my_levels = int(my_bins/2)
    
    ###
    
    ax[0, alpha].set_title(rf'$\downarrow$$\alpha_{{CE}}$ = {CE_parameter[alpha]}$\downarrow$')
    
    Z0,xedges,yedges=np.histogram2d(Mi_prim_A2/Mi_sec_A2,
                                    Mf_prim_A2/Mf_sec_A2,
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z0 = np.transpose(Z0)
    Z0[Z0==0] = np.nan
    
    cs0 = ax[0, alpha].contourf(x,y,Z0,levels=my_levels, cmap='Blues')
    
    k0 = 0
    t0 = 0
    for i in range(len(Mi_prim_A2)):
        if Mi_prim_A2[i]/Mi_sec_A2[i]<2 and Mf_prim_A2[i]/Mf_sec_A2[i]<2:
            t0+=1 
            if Mi_prim_A2[i]/Mi_sec_A2[i]>Mf_prim_A2[i]/Mf_sec_A2[i]:
                k0+=1
    
    ###
    
    Z1,xedges,yedges=np.histogram2d(Mi_prim_A4_2/Mi_sec_A4_2,
                                    Mf_prim_A4_2/Mf_sec_A4_2,
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z1 = np.transpose(Z1)
    Z1[Z1==0] = np.nan
    
    cs1 = ax[1, alpha].contourf(x,y,Z1,levels=my_levels, cmap='Greens')
    
    k1 = 0
    t1 = 0
    for i in range(len(Mi_prim_A4_2)):
        if Mi_prim_A4_2[i]/Mi_sec_A4_2[i]<2 and Mf_prim_A4_2[i]/Mf_sec_A4_2[i]<1.2:
            t1+=1 
            if Mi_prim_A4_2[i]/Mi_sec_A4_2[i]>Mf_prim_A4_2[i]/Mf_sec_A4_2[i]:
                k1+=1
    
    ###
    
    Z2,xedges,yedges=np.histogram2d(Mi_prim_A5_2/Mi_sec_A5_2,
                                    Mf_prim_A5_2/Mf_sec_A5_2,
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z2 = np.transpose(Z2)
    Z2[Z2==0] = np.nan
    
    cs2 = ax[2, alpha].contourf(x,y,Z2,levels=my_levels, cmap='Reds')
    
    k2 = 0
    t2 = 0
    for i in range(len(Mi_prim_A5_2)):
        if Mi_prim_A5_2[i]/Mi_sec_A5_2[i]<2 and Mf_prim_A5_2[i]/Mf_sec_A5_2[i]<2:
            t2+=1
            if Mi_prim_A5_2[i]/Mi_sec_A5_2[i]>Mf_prim_A5_2[i]/Mf_sec_A5_2[i]:
                k2+=1
    
    ###
    
    Z3,xedges,yedges=np.histogram2d(Mi_prim_B2/Mi_sec_B2,
                                    Mf_prim_B2/Mf_sec_B2,
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z3 = np.transpose(Z3)
    Z3[Z3==0] = np.nan
    
    cs3 = ax[3, alpha].contourf(x,y,Z3,levels=my_levels, cmap='Greys')
    
    k3 = 0
    t3 = 0
    for i in range(len(Mi_prim_B2)):
        if Mi_prim_B2[i]/Mi_sec_B2[i]<2 and Mf_prim_B2[i]/Mf_sec_B2[i]<2:
            t3+=1
            if Mi_prim_B2[i]/Mi_sec_B2[i]>Mf_prim_B2[i]/Mf_sec_B2[i]:
                k3+=1
    
    ###
    
    ax[0, alpha].plot([1,2],[1,2], c='darkviolet', lw=2)
    ax[1, alpha].plot([1,2],[1,2], c='darkviolet', lw=2)
    ax[2, alpha].plot([1,2],[1,2], c='darkviolet', lw=2)
    ax[3, alpha].plot([1,2],[1,2], c='darkviolet', lw=2)
    
    ax[2, alpha].plot([1.6,2],[1,1.4], c='black', lw=3, ls=(0,(4,8)))
    ax[2, alpha].plot([1.1,1.7],[1.2,2], c='black', lw=3, ls=(0,(1,8)))
    
    ax[0, alpha].set_ylim(1, 2)
    ax[1, alpha].set_ylim(1, 1.2)
    ax[2, alpha].set_ylim(1, 2)
    ax[3, alpha].set_ylim(1, 2)
    
    ax[0, alpha].set_xlim(1, 2)
    
    if alpha==0:
        ax[0, alpha].set_yticks([1,1.2,1.4,1.6,1.8,2], 
                                ['1.00','1.20','1.40','1.60','1.80','2.00'])
        ax[1, alpha].set_yticks([1,1.04,1.08,1.12,1.16,1.2], 
                                ['1.00','1.04','1.08','1.12','1.16','1.20'])
        ax[2, alpha].set_yticks([1,1.2,1.4,1.6,1.8,2], 
                                ['1.00','1.20','1.40','1.60','1.80','2.00'])
        ax[3, alpha].set_yticks([1,1.2,1.4,1.6,1.8,2], 
                                ['1.00','1.20','1.40','1.60','1.80','2.00'])
    else:
        ax[0, alpha].set_yticks([1,1.2,1.4,1.6,1.8,2], [])
        ax[1, alpha].set_yticks([1,1.04,1.08,1.12,1.16,1.2], [])
        ax[2, alpha].set_yticks([1,1.2,1.4,1.6,1.8,2], [])
        ax[3, alpha].set_yticks([1,1.2,1.4,1.6,1.8,2], [])
    
    ax[3, alpha].set_xticks([1,1.2,1.4,1.6,1.8,2], 
                            ['1.00','1.20','1.40','1.60','1.80','2.00'], 
                            rotation=60)
    
    # ax[3, alpha].set_xticks([1,1.2,1.4,1.6,1.8,2], 
    #                         ['1.0','1.2','1.4','1.6','1.8','2.0'], rotation=90)
    
    # ax[alpha, 0].set_yticks([1,1.5,2], ['1.0','1.5','2.0'])
    
    ax[0,0].set_ylabel(r'$\downarrow$A2$\downarrow$''\n'fr'CO={totals[0]}%')
    ax[1,0].set_ylabel(r'$\downarrow$A4-2$\downarrow$''\n'f'CO={totals[1]}%')
    ax[2,0].set_ylabel(r'$\downarrow$A5-2$\downarrow$''\n'f'CO={totals[2]}%')
    ax[3,0].set_ylabel(r'$\downarrow$B2$\downarrow$''\n'f'CO={totals[3]}%')
    
    ax[0,alpha].grid()
    ax[1,alpha].grid()
    ax[2,alpha].grid()
    ax[3,alpha].grid()
    
    ax[3,1].set_xlabel(r'$q_i$', fontsize=20)
    
    # ax[0, alpha].annotate(f'{round(k0*100/t0,2):.2f}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[1, alpha].annotate(f'{round(k1*100/t1,2):.2f}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[2, alpha].annotate(f'{round(k2*100/t2,2):.2f}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[3, alpha].annotate(f'{round(k3*100/t3,2):.2f}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    
    # ax[0, alpha].annotate(f'CO={ratios[alpha][0]}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[1, alpha].annotate(f'CO={ratios[alpha][1]}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[2, alpha].annotate(f'CO={ratios[alpha][2]}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[3, alpha].annotate(f'CO={ratios[alpha][3]}%', xy=(0.98,0.04), xytext=(0.98,0.04),
    #                       xycoords='axes fraction', ha='right', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    
fig.supylabel(r'$q_f$')

plt.tight_layout()

plt.subplots_adjust(hspace=0.15, wspace=0.08)

plt.savefig('plots/ZAMS_REM.png', dpi=200, bbox_inches='tight')

plt.show()

## 

fig, ax = plt.subplots(4,3,sharex=True,sharey=True)

for alpha in range(my_alpha):
    Mi_prim_A2 = []
    Mi_prim_A4_2 = []
    Mi_prim_A5_2 = []
    Mi_prim_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mi_prim_A2.append(CHA2_l[alpha][Z][system][0])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mi_prim_A4_2.append(CHA4_2_l[alpha][Z][system][0])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mi_prim_A5_2.append(CHA5_2_l[alpha][Z][system][0])
        for system in range(len(CHB2_l[alpha][Z])):
            Mi_prim_B2.append(CHB2_l[alpha][Z][system][0])
    
    Mi_prim_A2 = np.array(Mi_prim_A2, float)
    Mi_prim_A4_2 = np.array(Mi_prim_A4_2, float)
    Mi_prim_A5_2 = np.array(Mi_prim_A5_2, float)
    Mi_prim_B2 = np.array(Mi_prim_B2, float)
    
    # print(min(Mi_prim_A2), max(Mi_prim_A2), len(Mi_prim_A2))
    # print(min(Mi_prim_A4_2), max(Mi_prim_A4_2), len(Mi_prim_A4_2))
    # print(min(Mi_prim_A5_2), max(Mi_prim_A5_2), len(Mi_prim_A5_2))
    # print(min(Mi_prim_B2), max(Mi_prim_B2), len(Mi_prim_B2))
    # print('='*70)
    
    Mi_sec_A2 = []
    Mi_sec_A4_2 = []
    Mi_sec_A5_2 = []
    Mi_sec_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mi_sec_A2.append(CHA2_l[alpha][Z][system][1])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mi_sec_A4_2.append(CHA4_2_l[alpha][Z][system][1])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mi_sec_A5_2.append(CHA5_2_l[alpha][Z][system][1])
        for system in range(len(CHB2_l[alpha][Z])):
            Mi_sec_B2.append(CHB2_l[alpha][Z][system][1])
    
    Mi_sec_A2 = np.array(Mi_sec_A2, float)
    Mi_sec_A4_2 = np.array(Mi_sec_A4_2, float)
    Mi_sec_A5_2 = np.array(Mi_sec_A5_2, float)
    Mi_sec_B2 = np.array(Mi_sec_B2, float)
    
    # print(min(Mi_sec_A2), max(Mi_sec_A2), len(Mi_sec_A2))
    # print(min(Mi_sec_A4_2), max(Mi_sec_A4_2), len(Mi_sec_A4_2))
    # print(min(Mi_sec_A5_2), max(Mi_sec_A5_2), len(Mi_sec_A5_2))
    # print(min(Mi_sec_B2), max(Mi_sec_B2), len(Mi_sec_B2))
    # print('='*70)
    
    Mf_prim_A2 = []
    Mf_prim_A4_2 = []
    Mf_prim_A5_2 = []
    Mf_prim_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mf_prim_A2.append(CHA2_l[alpha][Z][system][2])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mf_prim_A4_2.append(CHA4_2_l[alpha][Z][system][2])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mf_prim_A5_2.append(CHA5_2_l[alpha][Z][system][2])
        for system in range(len(CHB2_l[alpha][Z])):
            Mf_prim_B2.append(CHB2_l[alpha][Z][system][2])
    
    Mf_prim_A2 = np.array(Mf_prim_A2, float)
    Mf_prim_A4_2 = np.array(Mf_prim_A4_2, float)
    Mf_prim_A5_2 = np.array(Mf_prim_A5_2, float)
    Mf_prim_B2 = np.array(Mf_prim_B2, float)
    
    # print(min(Mf_prim_A2), max(Mf_prim_A2), len(Mf_prim_A2))
    # print(min(Mf_prim_A4_2), max(Mf_prim_A4_2), len(Mf_prim_A4_2))
    # print(min(Mf_prim_A5_2), max(Mf_prim_A5_2), len(Mf_prim_A5_2))
    # print(min(Mf_prim_B2), max(Mf_prim_B2), len(Mf_prim_B2))
    # print('='*70)
    
    Mf_sec_A2 = []
    Mf_sec_A4_2 = []
    Mf_sec_A5_2 = []
    Mf_sec_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            Mf_sec_A2.append(CHA2_l[alpha][Z][system][3])
        for system in range(len(CHA4_2_l[alpha][Z])):
            Mf_sec_A4_2.append(CHA4_2_l[alpha][Z][system][3])
        for system in range(len(CHA5_2_l[alpha][Z])):
            Mf_sec_A5_2.append(CHA5_2_l[alpha][Z][system][3])
        for system in range(len(CHB2_l[alpha][Z])):
            Mf_sec_B2.append(CHB2_l[alpha][Z][system][3])
    
    Mf_sec_A2 = np.array(Mf_sec_A2, float)
    Mf_sec_A4_2 = np.array(Mf_sec_A4_2, float)
    Mf_sec_A5_2 = np.array(Mf_sec_A5_2, float)
    Mf_sec_B2 = np.array(Mf_sec_B2, float)
    
    # print(min(Mf_sec_A2), max(Mf_sec_A2), len(Mf_sec_A2))
    # print(min(Mf_sec_A4_2), max(Mf_sec_A4_2), len(Mf_sec_A4_2))
    # print(min(Mf_sec_A5_2), max(Mf_sec_A5_2), len(Mf_sec_A5_2))
    # print(min(Mf_sec_B2), max(Mf_sec_B2), len(Mf_sec_B2))
    # print('='*70)
    
    ai_A2 = []
    ai_A4_2 = []
    ai_A5_2 = []
    ai_B2 = []
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            ai_A2.append(CHA2_l[alpha][Z][system][4])
        for system in range(len(CHA4_2_l[alpha][Z])):
            ai_A4_2.append(CHA4_2_l[alpha][Z][system][4])
        for system in range(len(CHA5_2_l[alpha][Z])):
            ai_A5_2.append(CHA5_2_l[alpha][Z][system][4])
        for system in range(len(CHB2_l[alpha][Z])):
            ai_B2.append(CHB2_l[alpha][Z][system][4])
    
    ai_A2 = np.array(ai_A2, float)
    ai_A4_2 = np.array(ai_A4_2, float)
    ai_A5_2 = np.array(ai_A5_2, float)
    ai_B2 = np.array(ai_B2, float)
    
    # print(min(ai_A2), max(ai_A2), len(ai_A2))
    # print(min(ai_A4_2), max(ai_A4_2), len(ai_A4_2))
    # print(min(ai_A5_2), max(ai_A5_2), len(ai_A5_2))
    # print(min(ai_B2), max(ai_B2), len(ai_B2))
    # print('='*70)
    
    af_A2 = []
    af_A4_2 = []
    af_A5_2 = []
    af_B2 = []
    
    for Z in range(my_Z):
        for system in range(len(CHA2_l[alpha][Z])):
            af_A2.append(CHA2_l[alpha][Z][system][5])
        for system in range(len(CHA4_2_l[alpha][Z])):
            af_A4_2.append(CHA4_2_l[alpha][Z][system][5])
        for system in range(len(CHA5_2_l[alpha][Z])):
            af_A5_2.append(CHA5_2_l[alpha][Z][system][5])
        for system in range(len(CHB2_l[alpha][Z])):
            af_B2.append(CHB2_l[alpha][Z][system][5])
    
    af_A2 = np.array(af_A2, float)
    af_A4_2 = np.array(af_A4_2, float)
    af_A5_2 = np.array(af_A5_2, float)
    af_B2 = np.array(af_B2, float)
    
    # print(min(af_A2), max(af_A2), len(af_A2))
    # print(min(af_A4_2), max(af_A4_2), len(af_A4_2))
    # print(min(af_A5_2), max(af_A5_2), len(af_A5_2))
    # print(min(af_B2), max(af_B2), len(af_B2))
    # print('='*70)
    
    a_i = []
    a_f = []
    for i in range(len(ai_A2)):
        a_i.append(ai_A2[i])
    for i in range(len(ai_A4_2)):
        a_i.append(ai_A4_2[i])
    for i in range(len(ai_A5_2)):
        a_i.append(ai_A5_2[i])
    for i in range(len(ai_B2)):
        a_i.append(ai_B2[i])
    
    for i in range(len(af_A2)):
        a_f.append(af_A2[i])
    for i in range(len(af_A4_2)):
        a_f.append(af_A4_2[i])
    for i in range(len(af_A5_2)):
        a_f.append(af_A5_2[i])
    for i in range(len(af_B2)):
        a_f.append(af_B2[i])
    
    k = 0
    for i in range(len(a_i)):
        if a_i[i]<a_f[i]:
            k+=1
    
    print('======', round(k*100/len(a_i),2), round((len(a_i)-k)*100/len(a_i),2))
    
    my_bins = 350
    my_lw = 1
    my_levels = int(my_bins/2)
    
    ###
    
    Msun = 1.988e30
    Rsun = 6.957e8
    G = 6.6743e-11
    
    ax[0, alpha].set_title(rf'$\downarrow$$\alpha_{{CE}}$ = {CE_parameter[alpha]}$\downarrow$')
    
    Z0,xedges,yedges=np.histogram2d(-G*(Mi_prim_A2+Mi_sec_A2)*Msun/(2*ai_A2*Rsun),
                                    -G*(Mf_prim_A2+Mf_sec_A2)*Msun/(2*af_A2*Rsun),
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z0 = np.transpose(Z0)
    Z0[Z0==0] = np.nan
    # print(np.nanmax(y))
    cs0 = ax[0, alpha].contourf(x,y,Z0,levels=my_levels, cmap='Blues')
    
    k0 = 0
    for i in range(len(Mi_prim_A2)):
        if -G*(Mi_prim_A2[i]+Mi_sec_A2[i])*Msun/(2*ai_A2[i]*Rsun)>-G*(Mf_prim_A2[i]+Mf_sec_A2[i])*Msun/(2*af_A2[i]*Rsun):
            k0+=1
    
    ###
    
    Z1,xedges,yedges=np.histogram2d(-G*(Mi_prim_A4_2+Mi_sec_A4_2)*Msun/(2*ai_A4_2*Rsun),
                                    -G*(Mf_prim_A4_2+Mf_sec_A4_2)*Msun/(2*af_A4_2*Rsun),
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z1 = np.transpose(Z1)
    Z1[Z1==0] = np.nan
    # print(np.nanmax(y))
    cs1 = ax[1, alpha].contourf(x,y,Z1,levels=my_levels, cmap='Greens')
    
    k1 = 0
    for i in range(len(Mi_prim_A4_2)):
        if -G*(Mi_prim_A4_2[i]+Mi_sec_A4_2[i])*Msun/(2*ai_A4_2[i]*Rsun)>-G*(Mf_prim_A4_2[i]+Mf_sec_A4_2[i])*Msun/(2*af_A4_2[i]*Rsun):
            k1+=1
    
    ###
    
    Z2,xedges,yedges=np.histogram2d(-G*(Mi_prim_A5_2+Mi_sec_A5_2)*Msun/(2*ai_A5_2*Rsun),
                                    -G*(Mf_prim_A5_2+Mf_sec_A5_2)*Msun/(2*af_A5_2*Rsun),
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z2 = np.transpose(Z2)
    Z2[Z2==0] = np.nan
    # print(np.nanmax(y))
    cs2 = ax[2, alpha].contourf(x,y,Z2,levels=my_levels, cmap='Reds')
    
    k2 = 0
    for i in range(len(Mi_prim_A5_2)):
        if -G*(Mi_prim_A5_2[i]+Mi_sec_A5_2[i])*Msun/(2*ai_A5_2[i]*Rsun)>-G*(Mf_prim_A5_2[i]+Mf_sec_A5_2[i])*Msun/(2*af_A5_2[i]*Rsun):
            k2+=1
    
    ###
    
    Z3,xedges,yedges=np.histogram2d(-G*(Mi_prim_B2+Mi_sec_B2)*Msun/(2*ai_B2*Rsun),
                                    -G*(Mf_prim_B2+Mf_sec_B2)*Msun/(2*af_B2*Rsun),
                                    bins=my_bins,
                                    density=False)
    
    x=np.zeros(len(xedges)-1)
    y=np.zeros(len(xedges)-1)
    for i in range(len(xedges)-1):
        x[i]=(xedges[i]+xedges[i+1])/2.
        y[i]=(yedges[i]+yedges[i+1])/2.
    Z3 = np.transpose(Z3)
    Z3[Z3==0] = np.nan
    # print(np.nanmax(y))
    cs3 = ax[3, alpha].contourf(x,y,Z3,levels=my_levels, cmap='Greys')
    
    k3 = 0
    for i in range(len(Mi_prim_B2)):
        if -G*(Mi_prim_B2[i]+Mi_sec_B2[i])*Msun/(2*ai_B2[i]*Rsun)>-G*(Mf_prim_B2[i]+Mf_sec_B2[i])*Msun/(2*af_B2[i]*Rsun):
            k3+=1
    
    ###
    
    ax[0,alpha].plot([-1e11, -1e9], [-1e11, -1e9], c='darkviolet', lw=2)
    ax[1,alpha].plot([-1e11, -1e9], [-1e11, -1e9], c='darkviolet', lw=2)
    ax[2,alpha].plot([-1e11, -1e9], [-1e11, -1e9], c='darkviolet', lw=2)
    ax[3,alpha].plot([-1e11, -1e9], [-1e11, -1e9], c='darkviolet', lw=2)
    
    # ax[0, alpha].annotate(f'{round(k0*100/len(Mi_prim_A2),2)}%', xy=(0.02,0.04), xytext=(0.02,0.04),
    #                       xycoords='axes fraction', ha='left', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[1, alpha].annotate(f'{round(k1*100/len(Mi_prim_A4_2),2)}%', xy=(0.02,0.04), xytext=(0.02,0.04),
    #                       xycoords='axes fraction', ha='left', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[2, alpha].annotate(f'{round(k2*100/len(Mi_prim_A5_2),2)}%', xy=(0.02,0.04), xytext=(0.02,0.04),
    #                       xycoords='axes fraction', ha='left', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    # ax[3, alpha].annotate(f'{round(k3*100/len(Mi_prim_B2),2)}%', xy=(0.02,0.04), xytext=(0.02,0.04),
    #                       xycoords='axes fraction', ha='left', va='bottom', fontsize=13,
    #                       bbox=dict(boxstyle='round', fc='white', ec='black', pad=0.1))
    
    ax[0, alpha].set_xscale('symlog')
    ax[0, alpha].set_yscale('symlog')
    
    ax[0,0].set_ylabel(r'$\downarrow$A2$\downarrow$')
    ax[1,0].set_ylabel(r'$\downarrow$A4-2$\downarrow$')
    ax[2,0].set_ylabel(r'$\downarrow$A5-2$\downarrow$')
    ax[3,0].set_ylabel(r'$\downarrow$B2$\downarrow$')
    
    ax[0,alpha].grid()
    ax[1,alpha].grid()
    ax[2,alpha].grid()
    ax[3,alpha].grid()
    
    ax[3,1].set_xlabel(r'ZAMS $E_{orb}$ [$m^2/s^2$]', fontsize=20)
    
# fig.supxlabel(r'ZAMS specific orbital energy [$m^2/s^2$]')
fig.supylabel(r'BNSS $E_{orb}$ [$m^2/s^2$]')

plt.tight_layout()

plt.subplots_adjust(hspace=0.0, wspace=0.0)

plt.savefig('plots/specific_orbital_energy.png', dpi=200, bbox_inches='tight')

plt.show()

##############################################################################

CHA_l = [[],[],[]]
CHB_l = [[],[],[]]

CHA1_l = [[],[],[]]
CHA2_l = [[],[],[]]
CHA3_l = [[],[],[]]
CHA4_1_l = [[],[],[]]
CHA4_2_l = [[],[],[]]
CHA5_1_l = [[],[],[]]
CHA5_2_l = [[],[],[]]
CHA6_1_l = [[],[],[]]
CHA6_2_l = [[],[],[]]

CHB1_l = [[],[],[]]
CHB2_l = [[],[],[]]
CHB3_l = [[],[],[]]
CHB4_l = [[],[],[]]
CHB5_l = [[],[],[]]

CHC_l = [[],[],[]]

CHD_l = [[],[],[]]

CHE_l = [[],[],[]]

OTH_l = [[],[],[]]

for alpha in range(my_alpha):
    # test = 'plots/channels/Channels_alpha'+str(alpha)+'.png'
    # if exists(test)==True:
    #     print(f'>>> Plot Channels_alpha{alpha} exists already <<<')
    # if exists(test)==False:
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        CHA = []
        CHB = []
        
        CHA1 = []
        CHA2 = []
        CHA3 = []
        CHA4_1 = []
        CHA4_2 = []
        CHA5_1 = []
        CHA5_2 = []
        CHA6_1 = []
        CHA6_2 = []
        
        CHB1 = []
        CHB2 = []
        CHB3 = []
        CHB4 = []
        CHB5 = []
        
        CHC = []
        
        CHD = []
        
        CHE = []
        
        OTH = []
        
        
        for i in range(len(df)):
            t_RLO, t_CE, t_HE_0, t_HE_1, t_NS_0, t_NS_1 = df['t_RLO'][i],df['t_CE'][i],df['t_HE_0'][i],df['t_HE_1'][i],df['t_NS_0'][i],df['t_NS_1'][i]
                            
            # Canali di formazione
            if (t_RLO!=0. and
                t_CE!=0. and
                t_HE_0!=0. and
                t_HE_1!=0. and
                t_RLO<=t_HE_0 and
                t_HE_0<=t_HE_1 and
                t_HE_1<t_CE and
                t_CE<=t_NS_0):
                CHA1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_0):
                CHA1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA2.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<t_NS_1):
                CHA3.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_2.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_2.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_2.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_2.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<=t_CE and
                  t_CE<=t_NS_1):
                CHB2.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_1):
                CHB3.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_1):
                CHB4.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_NS_1):
                CHB5.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_CE!=0. and
                  t_RLO!=0. and
                  t_CE<t_RLO and
                  t_RLO<=t_NS_0):
                CHC.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_CE==0. and
                  t_RLO!=0. and
                  t_RLO<t_NS_0):
                CHC.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHD.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            
            elif (t_RLO!=0. and
                  t_CE==0. and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHE.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHE.append([df['t_CO_0'][i], df['t_CO_1'][i]])
            
            else:
                OTH.append([df['t_CO_0'][i], df['t_CO_1'][i]])
                # print(df['ID'][i])
        
        CHA_l[alpha].append(CHA1+CHA2+CHA3+CHA4_1+CHA4_2+CHA5_1+CHA5_2+CHA6_1+CHA6_2)
        CHB_l[alpha].append(CHB1+CHB2+CHB3+CHB4+CHB5)
        
        CHA1_l[alpha].append(CHA1)
        CHA2_l[alpha].append(CHA2)
        CHA3_l[alpha].append(CHA3)
        CHA4_1_l[alpha].append(CHA4_1)
        CHA4_2_l[alpha].append(CHA4_2)
        CHA5_1_l[alpha].append(CHA5_1)
        CHA5_2_l[alpha].append(CHA5_2)
        CHA6_1_l[alpha].append(CHA6_1)
        CHA6_2_l[alpha].append(CHA6_2)
        
        CHB1_l[alpha].append(CHB1)
        CHB2_l[alpha].append(CHB2)
        CHB3_l[alpha].append(CHB3)
        CHB4_l[alpha].append(CHB4)
        CHB5_l[alpha].append(CHB5)
        
        CHC_l[alpha].append(CHC)
        
        CHD_l[alpha].append(CHD)
        
        CHE_l[alpha].append(CHE)
        
        OTH_l[alpha].append(OTH)
        
        # Nota: i dati in queste liste sono formattati così:
        # M_i primaria, M_i secondaria, M_f primaria, M_f secondaria, a_i, a_f
        
        # 17 canali di formazione


def CO_enumerate(my_channel):
    counter = 0
    total = 0
    
    for alpha in range(my_alpha):
        for Z in range(my_Z):
            for i in range(len(my_channel[alpha][Z])):
                a,b = my_channel[alpha][Z][i][0],my_channel[alpha][Z][i][1]
                if a!=0:
                    counter+=1
                if b!=0:
                    counter+=1
            total+=len(my_channel[alpha][Z])*2
    
    print(round(counter*100/total,2))















#############################################################################
#############################################################################
#############################################################################

plt.rcParams.update({'font.size':15})

for alpha in range(my_alpha):
    CH1_list = []
    CH2_list = []
    CH3_list = []
    CH4_list = []
    CH5_list = []
    CH6_list = []
    OTH_list = []
    
    CHA_l = []
    CHB_l = []
    
    CHA1_l = []
    CHA2_l = []
    CHA3_l = []
    CHA4_1_l = []
    CHA4_2_l = []
    CHA5_1_l = []
    CHA5_2_l = []
    CHA6_1_l = []
    CHA6_2_l = []
    
    CHB1_l = []
    CHB2_l = []
    CHB3_l = []
    CHB4_l = []
    CHB5_l = []
    
    CHC_l = []
    
    CHD_l = []
    
    CHE_l = []
    
    OTH_l = []
    TOT_l = []
    
    # test = 'plots/channels/Channels_alpha'+str(alpha)+'.png'
    # if exists(test)==True:
    #     print(f'>>> Plot Channels_alpha{alpha} exists already <<<')
    # if exists(test)==False:
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        CHA = 0
        CHB = 0
        
        CHA1 = 0
        CHA2 = 0
        CHA3 = 0
        CHA4_1 = 0
        CHA4_2 = 0
        CHA5_1 = 0
        CHA5_2 = 0
        CHA6_1 = 0
        CHA6_2 = 0
        
        CHB1 = 0
        CHB2 = 0
        CHB3 = 0
        CHB4 = 0
        CHB5 = 0
        
        CHC = 0
        
        CHD = 0
        
        CHE = 0
        
        OTH = 0
        
        
        for i in range(len(df)):
            t_RLO, t_CE, t_HE_0, t_HE_1, t_NS_0, t_NS_1 = df['t_RLO'][i],df['t_CE'][i],df['t_HE_0'][i],df['t_HE_1'][i],df['t_NS_0'][i],df['t_NS_1'][i]
                            
            # Canali di formazione
            if (t_RLO!=0. and
                t_CE!=0. and
                t_HE_0!=0. and
                t_HE_1!=0. and
                t_RLO<=t_HE_0 and
                t_HE_0<=t_HE_1 and
                t_HE_1<t_CE and
                t_CE<=t_NS_0):
                CHA1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_0):
                CHA1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<t_NS_1):
                CHA3+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_1+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0):
                CHA4_2+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0):
                CHA4_2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_1+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_CE<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<=t_HE_1):
                CHA5_2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<t_CE and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_1+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_HE_0):
                CHA6_2+=1
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_HE_1 and
                  t_HE_1<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHB1+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_HE_1 and
                  t_HE_1<=t_CE and
                  t_CE<=t_NS_1):
                CHB2+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1!=0. and
                  t_RLO<=t_HE_0 and
                  t_HE_0<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_1 and
                  t_HE_1<t_NS_1):
                CHB3+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_HE_0 and
                  t_HE_0<t_CE and
                  t_CE<=t_NS_1):
                CHB4+=1
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_HE_0!=0. and
                  t_HE_1==0. and
                  t_RLO<=t_NS_0 and
                  t_NS_0<t_CE and
                  t_CE<=t_HE_0 and
                  t_HE_0<=t_NS_1):
                CHB5+=1
            
            elif (t_CE!=0. and
                  t_RLO!=0. and
                  t_CE<t_RLO and
                  t_RLO<=t_NS_0):
                CHC+=1
            elif (t_CE==0. and
                  t_RLO!=0. and
                  t_RLO<t_NS_0):
                CHC+=1
            
            elif (t_RLO!=0. and
                  t_CE!=0. and
                  t_CE<=t_NS_0 and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHD+=1
            
            elif (t_RLO!=0. and
                  t_CE==0. and
                  t_NS_0<t_RLO and
                  t_RLO<=t_NS_1):
                CHE+=1
            elif (t_RLO==0. and
                  t_CE!=0. and
                  t_NS_0<t_CE and
                  t_CE<=t_NS_1):
                CHE+=1
            
            else:
                OTH+=1
                # print(df['ID'][i])
            
        TOT_l.append(len(df)-OTH)
        
        CHA_l.append(CHA1+CHA2+CHA3+CHA4_1+CHA4_2+CHA5_1+CHA5_2+CHA6_1+CHA6_2)
        CHB_l.append(CHB1+CHB2+CHB3+CHB4+CHB5)
        
        CHA1_l.append(CHA1)
        CHA2_l.append(CHA2)
        CHA3_l.append(CHA3)
        CHA4_1_l.append(CHA4_1)
        CHA4_2_l.append(CHA4_2)
        CHA5_1_l.append(CHA5_1)
        CHA5_2_l.append(CHA5_2)
        CHA6_1_l.append(CHA6_1)
        CHA6_2_l.append(CHA6_2)
        
        CHB1_l.append(CHB1)
        CHB2_l.append(CHB2)
        CHB3_l.append(CHB3)
        CHB4_l.append(CHB4)
        CHB5_l.append(CHB5)
        
        CHC_l.append(CHC)
        
        CHD_l.append(CHD)
        
        CHE_l.append(CHE)
        
        OTH_l.append(OTH)
    
    print(CHA_l)
    print(CHA1_l, CHA2_l, CHA3_l, CHA4_1_l, CHA4_2_l, CHA5_1_l, CHA5_2_l, CHA6_1_l, CHA6_2_l)
    
    print('----')
    print(CHB_l)
    print(CHB1_l, CHB2_l, CHB3_l, CHB4_l, CHB5_l)
    
    print('----')
    print(CHC_l)
    
    print('----')
    print(CHD_l)
    
    print('----')
    print(CHE_l)
    
    print('----')
    print(OTH_l)
    
    print('='*70)
    
    channels = np.array([CHA1_l, CHA2_l, CHA3_l, CHA4_1_l, CHA4_2_l, CHA5_1_l, CHA5_2_l, CHA6_1_l, 
                CHA6_2_l, CHB1_l, CHB2_l, CHB3_l, CHB4_l, CHB5_l, CHC_l, CHD_l, CHE_l], int)
    
    names = np.array(['Channel','A1', 'A2', 'A3', 'A4-1', 'A4-2', 'A5-1', 'A5-2', 'A6-1', 
                'A6-2', 'B1', 'B2', 'B3', 'B4', 'B5', 'C', 'D', 'E'], str)
    
    TOT_l = np.array(TOT_l)
    
    CHA_l = np.array(CHA_l, int)
    CHB_l = np.array(CHB_l, int)
    
    y = [np.zeros(my_Z,int)]
    for i in range(1,len(channels)):
        y.append(sum(channels[1:i+1]))
    
    my_color = plt.cm.tab20(np.linspace(0,1,20))
    my_color = np.concatenate((my_color[0:2], my_color[3:14],my_color[14:15],my_color[2:3],
                                my_color[15:]))
    
    table_colors = np.zeros([18], object)
    for i in range(18):
        if i==0:
            table_colors[i]='w'
        elif i!=0:
            table_colors[i]=my_color[i-1]
    
    my_text = np.zeros([17,my_Z], object)
    for i in range(len(y)):
        for j in range(my_Z):
            temp = '{:.2f}'.format(channels[i][j]/TOT_l[j]*100)
            
            if temp=='0.00':
                temp = '-'
            
            my_text[i][j]=temp
    
    fig, ax = plt.subplots()
    
    ax.set_title(fr'Relative fraction of systems produced through the considered formation channels - $\alpha_{{CE}}$={CE_parameter[alpha]}''\n'
                  'Channel A: below blue dashed line - Channel B: below red dashed line')
    ax.set_xscale('log')
    
    ax.plot(metallicity, y[0]/TOT_l, c='black', lw=1)
    ax.fill_between(metallicity, np.zeros(my_Z,int)/TOT_l, y[0]/TOT_l, color=my_color[0])
    
    for i in range(1, len(y)):
        ax.plot(metallicity, y[i]/TOT_l, c='black', lw=1)
        
        if i==1:
            A2_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==3:
            A4_1_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==4:
            A4_2_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==5:
            A5_1_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==6:
            A5_2_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==10:
            B2_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==13:
            B5_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==14:
            C_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==15:
            D_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        elif i==16:
            E_ch = ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
        else:
            ax.fill_between(metallicity, y[i-1]/TOT_l, y[i]/TOT_l, color=my_color[i])
    
    ax.plot(metallicity, CHA_l/TOT_l, c='blue', ls='dashed', lw=5)
    ax.plot(metallicity, (CHA_l+CHB_l)/TOT_l, c='red', ls='dashed', lw=5)
    
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.yaxis.set_ticks(np.linspace(0, 1, 11))
    ax.set_xlim(metallicity[0], metallicity[-1])
    ax.set_ylim(0, 1)
    
    ax.set_xlabel(r'Metallicity $Z$')
    
    leg = ax.legend([A2_ch,A4_1_ch,A4_2_ch,A5_1_ch,A5_2_ch,
                      B2_ch,B5_ch,
                      C_ch,D_ch,E_ch], 
                    ['Channel A2','Channel A4-1','Channel A4-2','Channel A5-1','Channel A5-2',
                      'Channel B2','Channel B5',
                      'Channel C','Channel D','Channel E',], edgecolor='black', 
                    title='More relevant channels:',
                    framealpha=1, handler_map={matplotlib.lines.Line2D: SymHandler()}, 
                    handleheight=1, labelspacing=0.5, ncol=5, loc=(0,-0.19))
    leg._legend_box.align = "left"
    ax.grid(visible=True, which='major', axis='both', color='black', alpha=0.8, ls='dotted')
    ax.grid(visible=True, which='minor', axis='both', color='black', alpha=0.8, ls='dotted')
    
    my_header = np.array([[r'$Z_0$',r'$Z_1$',r'$Z_2$',r'$Z_3$',r'$Z_4$',r'$Z_5$',r'$Z_6$',r'$Z_7$',r'$Z_8$',r'$Z_9$',r'$Z_{10}$']], str)
    my_text = np.concatenate((my_header, my_text))
    the_table = plt.table(cellText=my_text, 
        rowLabels=names, bbox=(1.07,0,0.4,1), rowColours=table_colors, 
        cellLoc='center', zorder=2)
    the_table.auto_set_font_size(False)
    the_table.set_fontsize(10)
    
    plt.savefig('plots/Channels_alpha'+str(alpha)+'.png', dpi=200, bbox_inches='tight')
    
    plt.show()
    
    ############################################################################
    
    NK = []
    NK_broken = []
    for Z in range(my_Z):
        for counter in range(8):
            temp2 = np.genfromtxt(directory+str(alpha)+'_Z'+str(Z)+'/broken_NK.txt')
            for i in range(len(temp2)):
                NK_broken.append(temp2[i])
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        temp0 = df['NK_0']
        temp1 = df['NK_1']
        
        for i in range(len(df)):
            NK.append(temp0[i])
            NK.append(temp1[i])
    
    step = 25
    
    plt.title(f'Normalised distribution of SN natal kicks - Step of {step} km/s''\n'
                fr'$\alpha_{{CE}}$={CE_parameter[alpha]} - all $Z$')
    
    h = plt.hist(NK, bins=np.arange(((min(NK)//step)+1)*step, ((max(NK)//step)+1)*step,step),
                  rwidth=0.9, color='green', density=True, label='Binary neutron''\n''stars systems')
    h_broken = plt.hist(NK_broken, bins=np.arange(((min(NK_broken)//step)+1)*step, ((max(NK_broken)//step)+1)*step,step), 
                  rwidth=0.4, color='blue', density=True, label='Broken systems after''\n''a NS formation')
    
    plt.xlim(0, 1400)
    
    plt.xlabel('Modulus of natal kick [km/s]')
    
    plt.legend()
    plt.grid(axis='y')
    
    plt.savefig('plots/NK_distribution_alpha'+str(alpha)+'_total.png', dpi=200, bbox_inches='tight')
        
    plt.show()
    
    #############################################################################
    
    primary = []
    secondary = []
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        M0, M1 = df['M0_f'], df['M1_f']
        for i in range(len(df)):
            primary.append(max(M0[i], M1[i]))
            secondary.append(min(M0[i], M1[i]))
    
    xmin, xmax, ymin, ymax = 1.3,3.0,1.3,3.0
    resolution = 0.02
    
    fig, ax = plt.subplots()
    
    ax.set_title(fr'Histogram of primary VS secondary remnant (NS) mass - Grid resolution: {resolution}$M_\odot \times${resolution}$M_\odot$''\n'
                  fr'$\alpha_{{CE}}$={CE_parameter[alpha]} - all $Z$')
    
    h = ax.hist2d(primary, secondary,
                  bins=[int((xmax-xmin)/resolution),int((ymax-ymin)/resolution)],
                  range=[[xmin, xmax], [ymin, ymax]], norm=LogNorm(), cmap='plasma')
    fig.colorbar(h[3], ax=ax, label='Counts')
    
    ax.set_xlabel(r'Mass of primary [$M_\odot$]')
    ax.set_ylabel(r'Mass of secondary [$M_\odot$]')
    
    plt.tight_layout()
    
    plt.savefig('plots/hist2d_remnant_mass_alpha'+str(alpha)+'.png', dpi=200, bbox_inches="tight")
    
    plt.show()
    
    #############################################################################
    
    M_f = []
    for Z in range(my_Z):
        df = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')
        
        temp0 = df['M0_f']
        temp1 = df['M1_f']
        
        for i in range(len(df)):
            M_f.append(temp0[i])
            M_f.append(temp1[i])
    
    step = 0.02
    
    plt.title(fr'Normalised cumulative distribution of remannt mass in BNSS - Step of {step} $M_\odot$''\n'
              fr'$\alpha_{{CE}}$={CE_parameter[alpha]} - all $Z$')
    
    h = plt.hist(M_f, bins=np.arange(((min(M_f)//step)+1)*step, ((max(M_f)//step)+1)*step,step), 
                  color='black', histtype='step', cumulative=True, 
                  density=True)
    
    plt.xlim(h[1][0], h[1][-1])
    
    plt.grid(axis='y')
    
    plt.xlabel(r'Remnant mass [$M_\odot$]')
    
    plt.savefig('plots/Cumulative_remnant_alpha'+str(alpha)+'.png', dpi=200, bbox_inches='tight')
    
    plt.show()
    
    ############################################################################


