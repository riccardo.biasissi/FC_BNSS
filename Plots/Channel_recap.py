# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 10:04:13 2023

@author: ricca
"""

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':18})

y = 1

plt.figure(figsize=(20, 26))

# A channel
x0, x1 = 0, 11
t_RLO = 1
t_CE = 4
t_NS_0 = 7
t_NS_1 = 10

ax0 = plt.subplot(12,2,(1,2))
ax0.set_title('Channel A')
ax0.plot([x0,x1],[y,y], c='black', lw=3)
ax0.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=1000)
ax0.scatter([t_RLO],[y], c='black',zorder=2, marker='x', s=1000)
ax0.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=1000)
ax0.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=2000)
ax0.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=2000)

ax0.annotate('First stable''\n''mass transfer', xy=(t_RLO,y*1.03), xytext=(t_RLO,y*1.03), xycoords='data',
             ha='center')
ax0.annotate('Optional''\n''event', xy=(t_RLO,y*0.945), xytext=(t_RLO,y*0.945), xycoords='data',
             ha='center')
ax0.annotate('First unstable''\n''mass transfer', xy=(t_CE,y*1.03), xytext=(t_CE,y*1.03), xycoords='data',
             ha='center')
ax0.annotate('NS formation', xy=(t_NS_0,y*1.03), xytext=(t_NS_0,y*1.03), xycoords='data',
             ha='center')

ax0.set_xlim(x0-0.25,x1+0.25)
ax0.axis('off')

ax1 = plt.subplot(12,2,3)
ax1.set_title('Channel A1')
ax1.plot([x0,x1],[y,y], c='black', lw=3)
ax1.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax1.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax1.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax1.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax1.scatter((t_CE-t_RLO)/3+t_RLO, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax1.scatter(2*(t_CE-t_RLO)/3+t_RLO, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax1.scatter(2*(t_CE-t_RLO)/3+t_RLO, y, zorder=2, c='black', marker='x', s=700)

ax1.annotate('He-naked core(s)''\n''formation', xy=((t_CE-t_RLO)/3+t_RLO,y*1.011), xytext=((t_CE-t_RLO)/2+t_RLO,y*1.03), xycoords='data',
             ha='center',
            arrowprops=dict(arrowstyle='->, widthB=3.0, lengthB=1.5', lw=1.0))
ax1.annotate('He-naked core(s)''\n''formation', xy=(2*(t_CE-t_RLO)/3+t_RLO,y*1.011), xytext=((t_CE-t_RLO)/2+t_RLO,y*1.03), xycoords='data',
             ha='center',
            arrowprops=dict(arrowstyle='->, widthB=3.0, lengthB=1.5', lw=1.0))
# ax1.annotate('Optional', xy=(2*(t_CE-t_RLO)/3+t_RLO,y*0.97), xytext=(2*(t_CE-t_RLO)/3+t_RLO,y*0.97), xycoords='data',
#               ha='center')

ax1.axis('off')

ax2 = plt.subplot(12,2,4)
ax2.set_title('Channel A2')
ax2.plot([x0,x1],[y,y], c='black', lw=3)
ax2.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax2.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax2.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax2.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax2.scatter((t_CE-t_RLO)/2+t_RLO, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax2.scatter((t_NS_0-t_CE)/2+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax2.axis('off')

ax3 = plt.subplot(12,2,5)
ax3.set_title('Channel A3')
ax3.plot([x0,x1],[y,y], c='black', lw=3)
ax3.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax3.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax3.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax3.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax3.scatter((t_CE-t_RLO)/2+t_RLO, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax3.scatter((t_NS_1-t_NS_0)/2+t_NS_0, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax3.axis('off')

ax4 = plt.subplot(12,2,6)
ax4.set_title('Channel A4-1')
ax4.plot([x0,x1],[y,y], c='black', lw=3)
ax4.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax4.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax4.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax4.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax4.scatter((t_NS_0-t_CE)/3+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax4.scatter(2*(t_NS_0-t_CE)/3+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax4.scatter(2*(t_NS_0-t_CE)/3+t_CE, y, zorder=2, c='black', marker='x', s=700)
ax4.axis('off')

ax5 = plt.subplot(12,2,7)
ax5.set_title('Channel A4-2')
ax5.plot([x0,x1],[y,y], c='black', lw=3)
ax5.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax5.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax5.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax5.scatter((t_NS_0-t_CE)/3+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax5.scatter(2*(t_NS_0-t_CE)/3+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax5.scatter(2*(t_NS_0-t_CE)/3+t_CE, y, zorder=2, c='black', marker='x', s=700)
ax5.axis('off')

ax6 = plt.subplot(12,2,8)
ax6.set_title('Channel A5-1')
ax6.plot([x0,x1],[y,y], c='black', lw=3)
ax6.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax6.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax6.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax6.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax6.scatter((t_NS_0-t_CE)/2+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax6.scatter((t_NS_1-t_NS_0)/2+t_NS_0, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax6.axis('off')

ax7 = plt.subplot(12,2,9)
ax7.set_title('Channel A5-2')
ax7.plot([x0,x1],[y,y], c='black', lw=3)
ax7.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax7.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax7.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax7.scatter((t_NS_0-t_CE)/2+t_CE, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax7.scatter((t_NS_1-t_NS_0)/2+t_NS_0, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax7.axis('off')

ax8 = plt.subplot(12,2,10)
ax8.set_title('Channel A6-1')
ax8.plot([x0,x1],[y,y], c='black', lw=3)
ax8.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax8.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax8.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax8.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax8.scatter((t_NS_1-t_NS_0)/2+t_NS_0, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax8.axis('off')

ax9 = plt.subplot(12,2,11)
ax9.set_title('Channel A6-2')
ax9.plot([x0,x1],[y,y], c='black', lw=3)
ax9.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax9.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax9.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax9.scatter((t_NS_1-t_NS_0)/2+t_NS_0, y, zorder=2, c='orange', edgecolors='black', marker='o', s=700)
ax9.axis('off')

plt.tight_layout()

# plt.savefig('Channel_A.png', dpi=150, bbox_inches='tight')

# plt.show()

# B and other channels
x0, x1 = 0, 11
t_RLO = 1
t_CE = 7
t_NS_0 = 4
t_NS_1 = 10

ax0 = plt.subplot(12,2,(13,14))
ax0.set_title('Channel B')
ax0.plot([x0,x1],[y,y], c='black', lw=3)
ax0.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=1000)
ax0.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=2000)
ax0.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=1000)
ax0.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=2000)

# ax0.annotate('First stable''\n''mass transfer', xy=(t_RLO,y*1.03), xytext=(t_RLO,y*1.03), xycoords='data',
#              ha='center')
# ax0.annotate('First unstable''\n''mass transfer', xy=(t_CE,y*1.03), xytext=(t_CE,y*1.03), xycoords='data',
#              ha='center')
# ax0.annotate('NS formation', xy=(t_NS_0,y*1.03), xytext=(t_NS_0,y*1.03), xycoords='data',
#              ha='center')

ax0.set_xlim(x0-0.25,x1+0.25)
ax0.axis('off')

ax1 = plt.subplot(12,2,15)
ax1.set_title('Channel B1')
ax1.plot([x0,x1],[y,y], c='black', lw=3)
ax1.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax1.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax1.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax1.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax1.scatter((t_NS_0-t_RLO)/3+t_RLO, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)
ax1.scatter(2*(t_NS_0-t_RLO)/3+t_RLO, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)
ax1.scatter(2*(t_NS_0-t_RLO)/3+t_RLO, y, c='black',zorder=2, marker='x', s=700)

# ax1.annotate('He-naked core(s)''\n''formation', xy=((t_NS_0-t_RLO)/3+t_RLO,y*1.011), xytext=((t_NS_0-t_RLO)/2+t_RLO,y*1.03), xycoords='data',
#              ha='center',
#             arrowprops=dict(arrowstyle='->, widthB=3.0, lengthB=1.5', lw=1.0))
# ax1.annotate('He-naked core(s)''\n''formation', xy=(2*(t_NS_0-t_RLO)/3+t_RLO,y*1.011), xytext=((t_NS_0-t_RLO)/2+t_RLO,y*1.03), xycoords='data',
#              ha='center',
#             arrowprops=dict(arrowstyle='->, widthB=3.0, lengthB=1.5', lw=1.0))
# ax1.annotate('Optional', xy=(2*(t_NS_0-t_RLO)/3+t_RLO,y*0.97), xytext=(2*(t_NS_0-t_RLO)/3+t_RLO,y*0.97), xycoords='data',
#              ha='center')

ax1.axis('off')

ax2 = plt.subplot(12,2,16)
ax2.set_title('Channel B2')
ax2.plot([x0,x1],[y,y], c='black', lw=3)
ax2.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax2.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax2.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax2.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax2.scatter((t_NS_0-t_RLO)/2+t_RLO, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)
ax2.scatter((t_CE-t_NS_0)/2+t_NS_0, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)

ax2.axis('off')

ax3 = plt.subplot(12,2,17)
ax3.set_title('Channel B3')
ax3.plot([x0,x1],[y,y], c='black', lw=3)
ax3.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax3.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax3.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax3.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax3.scatter((t_NS_0-t_RLO)/2+t_RLO, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)
ax3.scatter((t_NS_1-t_CE)/2+t_CE, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)

ax3.axis('off')

ax4 = plt.subplot(12,2,18)
ax4.set_title('Channel B4')
ax4.plot([x0,x1],[y,y], c='black', lw=3)
ax4.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax4.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax4.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax4.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax4.scatter((t_CE-t_NS_0)/2+t_NS_0, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)

ax4.axis('off')

ax5 = plt.subplot(12,2,19)
ax5.set_title('Channel B5')
ax5.plot([x0,x1],[y,y], c='black', lw=3)
ax5.scatter([t_RLO],[y], c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax5.scatter([t_NS_0],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax5.scatter([t_CE],[y], c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax5.scatter([t_NS_1],[y], c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax5.scatter((t_NS_1-t_CE)/2+t_CE, y, c='orange', edgecolors='black',zorder=2, marker='o', s=700)

ax5.axis('off')

ax6 = plt.subplot(12,2,21)
ax6.set_title('Channel C')
ax6.plot([x0,x1],[y,y], c='black', lw=3)
ax6.scatter(1,y, c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax6.scatter(1,y, c='black',zorder=2, marker='x', s=700)
ax6.scatter(4,y, c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax6.scatter(7,y, c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax6.scatter(10,y, c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)

ax6.axis('off')

ax7 = plt.subplot(12,2,22)
ax7.set_title('Channel D')
ax7.plot([x0,x1],[y,y], c='black', lw=3)
ax7.scatter(1,y, c='red', edgecolors='black',zorder=2, marker='s', s=700)
ax7.scatter(7,y, c='green', edgecolors='black',zorder=2, marker='d', s=700)
ax7.scatter(4,y, c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)
ax7.scatter(10,y, c='royalblue', edgecolors='black',zorder=2, marker='*', s=1500)

ax7.axis('off')

ax8 = plt.subplot(12,2,23)
ax8.set_title('Channel E')
ax8.plot([x0,x1],[y,y], c='black', lw=3)
ax8.scatter(1,y, c='royalblue', edgecolors='black', zorder=2, marker='*', s=1500)
ax8.scatter(11/2,y, facecolors='grey', edgecolors='black', zorder=2, marker='D', s=700)
ax8.scatter(10,y, c='royalblue', edgecolors='black', zorder=2, marker='*', s=1500)

ax8.axis('off')

# ax9 = plt.subplot(12,2,12)
# ax9.set_title('Channel E')
# ax9.plot([x0,x1],[y,y], c='black', lw=3)
# ax9.scatter(1,y, c='royalblue', zorder=2, marker='*', s=700)
# ax9.scatter(11/2,y, facecolors='green', edgecolors='black', zorder=2, marker='d', s=700)
# ax9.scatter(10,y, c='royalblue', zorder=2, marker='*', s=700)

ax8.annotate('Only stable/unstable''\n''mass transfer(s)', xy=(11/2,y*0.97), xytext=(11/2,y*0.95), 
              xycoords='data', ha='center')

# ax9.axis('off')










plt.tight_layout()

plt.savefig('Channels_scheme.png', dpi=200, bbox_inches='tight')

plt.show()


