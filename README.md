# Formation Channels of BNSS

This is a README file that explain the contents of this project. It is organised into 4 main sections:
- "Output" folder
- "Plots" folder
- "SEVN_NS.py" python script
- "plot.py" python script
- "Report - Formation Channels of Binary Neutron Stars.pdf"

## SEVN_NS.py

This python script contains initially a part that consists of the systems filtering (note that counter was set to 8 due to usage of multithreading while generating the SEVN output files). 

Then we created a "master.txt" files with the same type of informations of "logfile.dat" but only about the selected systems. This was done to speed up any further analysis and prevent us from searching completely through the "logfile.dat" all the times.

A third part consists of an enumeration of NS that were formed following a CCSN or an ECSN. Here we also identified the systems that undergoes more than 2 supernova events (due to a known issue within SEVN).

In the fourth section we created a "my_data.csv" file that contains only the time parameters that were relevant for our analysis (time of first stable mass transfer, first unstable mass transfer, formation of HE and CO-naked cores, formation of NS), the initial and final semi-major axis and modulus of natal kicks). When a time is set to 0, this means it does not exists

Finally we create a file that contains all the moduli natal kicks that refers to supernovae events which generate a NS but after which the binary system is broken

## plot.py

This python script is the one used to generate all the plots that are in the folder "Plots". In this folder there is also a "Channel_recap.py" which is a python script that generates the summary figures of all considered formation channels

## Folders

The folder "plot" contains the plots generated through "SEVN_plot.py", while the "Output" folder contains only the files generate from "SEVN_NS.py" (we do not uploaded the SEVN output files due to their large total size of more than 100GB)

## Report

The file "Report - Formation Channels of Binary Neutron Stars.pdf" is the final report that contains the methodology used and the discussion of this project