# -*- coding: utf-8 -*-
"""
Created on Thu Jan  5 10:48:26 2023

@author: ricca
"""

import numpy as np
import re
import pandas as pd
from os.path import exists

# Valori cartelle, i my_parameter fanno riferimento al nome della cartella per
# cui alpha1_Z5 per essere analizzata richiederà almeno my_alpha = 1 e my_Z = 5

my_alpha=2
my_alpha+=1

my_Z=10
my_Z+=1

directory = 'E:/SEVN_work_dir/SEVN_outputs/sevn_output_alpha'

for alpha in range(my_alpha):
    for Z in range(my_Z):
        test = directory+str(alpha)+'_Z'+str(Z)+'/selected_0.txt'
        if exists(test)==True:
            print(f'>>> File selected_X.txt in directory alpha{alpha}_Z{Z} exists already! <<<')
        if exists(test)==False:
            my_sum = 0
            print(f'>>> Analysing {Z+1}° folder <<<')
            for counter in range(8):
                fselected = directory+str(alpha)+'_Z'+str(Z)+'/selected_'+str(counter)+'.txt'
                selected = open(fselected, 'w')
                
                logfile_path = directory+str(alpha)+'_Z'+str(Z)+'/logfile_'+str(counter)+'.dat'
                
                # Individuo tutte le NS formate e ne traccio l'ID del sistema
                
                print('--- Analysis of logfile_'+str(counter)+'.dat ---')
                
                regex_str = 'S;(.*?);.;NS'
                with open(logfile_path, 'r') as fo:
                    NS_sys = re.findall(regex_str, fo.read())
                print(f'There are {len(NS_sys)} singular NS')
                
                # Individuo tra questi sistemi quelli con due NS
                
                double_NS_sys = []
                for i in range(1, len(NS_sys)):
                    temp0, temp1 = NS_sys[i-1], NS_sys[i]
                    if temp0==temp1:
                        double_NS_sys.append(temp0)
                
                print(f'There are {len(double_NS_sys)} systems that have two NS')
                
                # Individuo tra questi i sistemi che risultano ancora uniti dopo una BSN
                # evitando di contare due volte quelli che vanno incontro a triple SN
                
                intact_BSN_sys = []
                
                regex_str = 'B;(.*?);.*?;BSN;.*:(.*)'
                with open (logfile_path, 'r') as fo:
                    temp = re.findall(regex_str, fo.read())
                
                for i in range(1, len(temp)-1):
                    temp0, temp1, check = temp[i-1], temp[i], temp[i+1]
                    if check[0]==temp0[0] and check[0]==temp1[0]:
                        intact_BSN_sys.append(temp[i][0])
                        i = i+2
                    if temp0[0]==temp1[0]:
                        if temp0[1]!='nan' and temp1[1]!='nan':
                            intact_BSN_sys.append(temp[i][0])
                
                double_NS_intact_sys = list(set(double_NS_sys).intersection(intact_BSN_sys))
                
                print(f'Among the latter ones, there are {len(double_NS_intact_sys)} systems intacts')
                
                # Concludo creando una lista con gli ID dei sistemi trovati in
                # un file txt
                
                for i in range(len(double_NS_intact_sys)):
                    selected.write(str(double_NS_intact_sys[i])+'\n')
                
                my_sum+=len(double_NS_intact_sys)
                selected.close()
            print(f'--- Found {my_sum} systems that are still intact after SN, with 2 NS and not coalescent ---')

print('='*70)

#################################################################################

# Creo un file master_X.txt per ogni selected_X.txt che contenga la lista degli
# eventi presenti in logfile_X.dat

for alpha in range(my_alpha):
    for Z in range(my_Z):
        test = directory+str(alpha)+'_Z'+str(Z)+'/master_0.txt'
        if exists(test)==True:
            print(f'>>> File master_X.txt in directory alpha{alpha}_Z{Z} exists already! <<<')
        if exists(test)==False:
            for counter in range(8):
                fmaster = directory+str(alpha)+'_Z'+str(Z)+'/master_'+str(counter)+'.txt'
                master = open(fmaster, 'w')
                logfile_path = directory+str(alpha)+'_Z'+str(Z)+'/logfile_'+str(counter)+'.dat'
                double_NS_intact_separated = np.genfromtxt(directory+str(alpha)+'_Z'+str(Z)+'/selected_'+str(counter)+'.txt', dtype='int64')
                
                for i in range(len(double_NS_intact_separated)):
                    regex_str = '(.;'+str(double_NS_intact_separated[i])+';.*)'
                    with open(logfile_path, 'r') as fo:
                        temp = re.findall(regex_str, fo.read())
                    for j in range(len(temp)):
                        master.write(temp[j]+'\n')
                    print(f'{alpha+1}/{my_alpha} - {Z+1}/{my_Z} - {counter+1}/8 - {i+1}/{len(double_NS_intact_separated)}')
                master.close()

print('='*70)

#################################################################################

# Analizzo i sistemi che ho salvato in master_X.txt per trovare le CCNS ed ECNS,
# quindi salvo il totale in un data_recap.txt. Salvo i sistemi che compiono 3 SN
# in un file triple_NS_sys.txt

for alpha in range(my_alpha):
    for Z in range(my_Z):
        test = directory+str(alpha)+'_Z'+str(Z)+'/data_recap.txt'
        
        if exists(test)==True:
            print(f'>>> File data_recap.txt in directory alpha{alpha}_Z{Z} exists already <<<')
        
        if exists(test)==False:
            print(f'>>> Analysing {Z+1}° folder <<<')
            data_triple = directory+str(alpha)+'_Z'+str(Z)+'/triple_NS_sys.txt'
            triple = open(data_triple, 'w')
            triple_list = []
            
            total = 0
            electron_capture_t = 0
            core_collapse_t = 0
            other_t = 0
            data_recap = directory+str(alpha)+'_Z'+str(Z)+'/data_recap.txt'
            recap = open(data_recap, 'w')
            
            for counter in range(8):
                logfile_path = directory+str(alpha)+'_Z'+str(Z)+'/master_'+str(counter)+'.txt'
                
                # Individuo le CCNS (identificativo 5) ed evito di contare doppio
                # nei sistemi a tripla NS
                
                for star in range(2):
                    triple_number = 0
                    regex_str = '.;(.*?);'+str(star)+';NS;.*?;5:'
                    with open(logfile_path, 'r') as fo:
                        CCNS = re.findall(regex_str, fo.read())
                    
                    for i in range(1,len(CCNS)):
                        if (CCNS[i]==CCNS[i-1]):
                            triple_list.append([CCNS[i], star])
                            triple_number+=1
                            print(f'Problematic: {CCNS[i]}')
                    
                    core_collapse_t+=len(CCNS)-triple_number
                
                
                # Individuo le ECNS (identificativo 4) ed evito di contare doppio
                # nei sistemi a tripla NS
                
                for star in range(2):
                    triple_number = 0
                    regex_str = '.;(.*?);'+str(star)+';NS;.*?;4:'
                    with open(logfile_path, 'r') as fo:
                        ECNS = re.findall(regex_str, fo.read())
                    
                    for i in range(1,len(ECNS)):
                        if (ECNS[i]==ECNS[i-1]):
                            triple_list.append([CCNS[i], star])
                            triple_number+=1
                            print(f'Problematic: {ECNS[i]}')
                    
                    electron_capture_t+=len(ECNS)-triple_number
                
                
                total = electron_capture_t+core_collapse_t
                
            # print(core_collapse_t)
            # print(electron_capture_t)
            # print(f'Total {total}')
            # print('-----------------')
            
            recap.write('Electron-capture-NS'+'\t'+str(electron_capture_t)+'\n')
            recap.write('Core-collapse-NS'+'\t'+str(core_collapse_t)+'\n')
            recap.write('Other-NS'+'\t\t'+str(other_t)+'\n')
            recap.write('Total'+'\t\t\t'+str(total))
            
            for i in range(len(triple_list)):
                triple.write(str(triple_list[i][0])+','+str(triple_list[i][1])+'\n')
            
            triple.close()
            recap.close()
            print(f'>>> Created data_recap.txt in directory alpha{alpha}_Z{Z} successfully <<<')
                
print('='*70)

#################################################################################

# Mi estrapolo i tempi caratteristici che mi servono per identificare i canali di formazione
# sono 8: t_RLO, t_CE, t_SN_0/1, t_HE_0/1, t_CO_0/1. Se non esistono, sono settati a zero.
# Tutti questi valori vengono salvati in un dataframe per comodità di utilizzo

for alpha in range(my_alpha):
    for Z in range(my_Z):
        test = directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv'
        if exists(test)==True:
            print(f'>>> File my_data.csv in directory alpha{alpha}_Z{Z} exists already <<<')
        if exists(test)==False:
            my_columns = ['ID', 't_RLO', 't_CE', 't_HE_0', 't_HE_1', 't_CO_0', 't_CO_1', 't_NS_0', 
                          't_NS_1', 'M0_i', 'M1_i', 'a_i', 'a_f', 'M0_f', 'M1_f', 'NK_0', 'NK_1']
            print(f'>>> Analysing {Z+1}° folder <<<')
            my_data = []
            
            for counter in range(8):
                logfile_path = directory+str(alpha)+'_Z'+str(Z)+'/master_'+str(counter)+'.txt'
                double_NS_intact_separated = np.genfromtxt(directory+str(alpha)+'_Z'+str(Z)+'/selected_'+str(counter)+'.txt', dtype='int64')
                
                output = pd.read_csv(directory+str(alpha)+'_Z'+str(Z)+'/output_'+str(counter)+'.csv')
                
                for i in range(len(double_NS_intact_separated)):
                    system = double_NS_intact_separated[i]
                    
                    # Individuo il primo t_RLO, prima determino tutti gli eventi RLO_BEGIN 
                    # e poi prendo il primo che non contiene un CE
                    
                    regex_str = '(.;'+str(system)+';.*RLO_BEGIN;(.*?);[\s\S]*?RLO_END.*)'
                    with open(logfile_path, 'r') as fo:
                        RLO_list = re.findall(regex_str, fo.read())
                    for j in range(len(RLO_list)):
                        regex_str = '.*?;CE;(.*?);'
                        check = re.findall(regex_str, RLO_list[j][0])
                        if len(check)!=0:
                            t_RLO = 0.
                        if len(check)==0:
                            t_RLO = float(RLO_list[j][1])
                            break
                    
                    # Individuo il primo t_CE
                    
                    regex_str = '.;'+str(system)+';.*?;CE;(.*?);'
                    with open(logfile_path, 'r') as fo:
                        CE_list = re.findall(regex_str, fo.read())
                    if len(CE_list)==0:
                        t_CE = 0.
                    if len(CE_list)!=0:
                        t_CE = float(CE_list[0])
                    
                    # Individuo la formazione dei nuclei di He-naked
                    
                    regex_str = '.;'+str(system)+';(.);HENAKED;(.*?);'
                    with open(logfile_path, 'r') as fo:
                        HE_list = re.findall(regex_str, fo.read())
                    if len(HE_list)==3:
                        t_HE_true = []
                        for star in range(2):
                            temp = []
                            for j in range(len(HE_list)):
                                if HE_list[j][0]==str(star):
                                    temp.append(float(HE_list[j][1]))
                            if len(temp)==2:
                                t_HE_true.append(max(temp))
                            if len(temp)==1:
                                t_HE_true.append(temp[0])
                        t_HE_0, t_HE_1 = min(t_HE_true), max(t_HE_true)
                    if len(HE_list)==2:
                        t_HE_0, t_HE_1 = float(HE_list[0][1]), float(HE_list[1][1])
                    if len(HE_list)==1:
                        t_HE_0, t_HE_1 = float(HE_list[0][1]), 0.
                    if len(HE_list)==0:
                        t_HE_0, t_HE_1 = 0., 0.
                    
                    # Individuo la formazione dei nuclei di CO-naked
                    
                    regex_str = '.;'+str(system)+';(.);CONAKED;(.*?);'
                    with open(logfile_path, 'r') as fo:
                        CO_list = re.findall(regex_str, fo.read())
                    
                    if len(CO_list)==3:
                        t_CO_true = []
                        for star in range(2):
                            temp = []
                            for j in range(len(CO_list)):
                                if CO_list[j][0]==str(star):
                                    temp.append(float(CO_list[j][1]))
                            if len(temp)==2:
                                t_CO_true.append(max(temp))
                            if len(temp)==1:
                                t_CO_true.append(temp[0])
                        t_CO_0, t_CO_1 = min(t_CO_true), max(t_CO_true)
                    if len(CO_list)==2:
                        t_CO_0, t_CO_1 = float(CO_list[0][1]), float(CO_list[1][1])
                    if len(CO_list)==1:
                        t_CO_0, t_CO_1 = float(CO_list[0][1]), 0.
                    if len(CO_list)==0:
                        t_CO_0, t_CO_1 = 0., 0.
                    
                    # Individuo la formazione delle due NS
                    
                    regex_str = '.;'+str(system)+';(.);NS;(.*?);'
                    with open(logfile_path, 'r') as fo:
                        NS_list = re.findall(regex_str, fo.read())
                    
                        # Nel caso ci fossero solo due SN, va bene e prendo i due valori
                    
                    if len(NS_list)==2:
                        t_NS_0, t_NS_1 = float(NS_list[0][1]), float(NS_list[1][1])
                    
                        # Nel caso ci fossero tre SN, dovrei prendere il secondo tempo (perchè?)
                        # di quella esplosa due volte
                    
                    if len(NS_list)==3:
                        # print(NS_list)
                        t_NS_true = []
                        for star in range(2):
                            temp = []
                            for j in range(len(NS_list)):
                                if NS_list[j][0]==str(star):
                                    temp.append(float(NS_list[j][1]))
                            if len(temp)==2:
                                t_NS_true.append(max(temp))
                            if len(temp)==1:
                                t_NS_true.append(temp[0])
                        # print(temp)
                        t_NS_0, t_NS_1 = min(t_NS_true), max(t_NS_true)
                        # print(t_NS_0, t_NS_1)
                    
                    
                    
                    # Individuo i valori di natal kick
                    
                    regex_str = '.;'+str(system)+';(.);SN;.+?:.*?:.*?:.*?:.*?:.*?:(.*?):'
                    with open(logfile_path, 'r') as fo:
                        NK_list = re.findall(regex_str, fo.read())
                    
                    if len(NK_list)==2:
                        NK_0, NK_1 = float(NK_list[0][1]), float(NK_list[1][1])
                    
                    if len(NK_list)==3:
                        NK_true = []
                        for star in range(2):
                            temp = []
                            for j in range(len(NK_list)):
                                if NK_list[j][0]==str(star):
                                    temp.append(float(NK_list[j][1]))
                            if len(temp)==2:
                                NK_true.append(max(temp))
                            if len(temp)==1:
                                NK_true.append(temp[0])
                        NK_0, NK_1 = min(NK_true), max(NK_true)
                    
                    # Individuo i valori di massa iniziale, finale e semiasse maggiore iniziale
                    
                    partial = output[output['name']==system]
                    M0_i = partial['Mass_0'].iloc[0]
                    M1_i = partial['Mass_1'].iloc[0]
                    a_i = partial['Semimajor'].iloc[0]
                    a_f = partial['Semimajor'].iloc[-1]
                    M0_f = partial[(partial['RemnantType_0']==4) | (partial['RemnantType_0']==5)]['Mass_0'].iloc[-1]
                    M1_f = partial[(partial['RemnantType_1']==4) | (partial['RemnantType_1']==5)]['Mass_1'].iloc[-1]
                    
                    my_data.append([system, t_RLO, t_CE, t_HE_0, t_HE_1, t_CO_0, t_CO_1, 
                                    t_NS_0, t_NS_1, M0_i, M1_i, a_i, a_f, M0_f, M1_f, NK_0, NK_1])
                    
                    if (i+1)%100==0:
                        print(f'{Z+1}/{my_Z} - {counter+1}/8 - {i+1}/{len(double_NS_intact_separated)}')
                    
                    
            
            df = pd.DataFrame(my_data,columns=my_columns)
            # print(df)
            df.to_csv(directory+str(alpha)+'_Z'+str(Z)+'/my_data.csv')

print('='*70)

#################################################################################

# Mi creo un file con i natal kick di tutte le SN che generano NS e rompono il sistema

for alpha in range(my_alpha):
    for Z in range(my_Z):
        test = directory+str(alpha)+'_Z'+str(Z)+'/broken_NK.txt'
        if exists(test)==True:
            print(f'>>> File broken_NS.txt in directory alpha{alpha}_Z{Z} already exists <<<')
        else:
            print(f'>>> Analysing {Z+1}° folder <<<')
            fbroken_NK = directory+str(alpha)+'_Z'+str(Z)+'/broken_NK.txt'
            broken_NK_file = open(fbroken_NK, 'w')
            for counter in range(8):
                regex_str = 'SN;.*?:.*?:.*?:.*?:[4-5]:.*?:(.*?):.*?[\s\S].*?BSN.*:nan'
                logfile_path = directory+str(alpha)+'_Z'+str(Z)+'/logfile_'+str(counter)+'.dat'
                with open(logfile_path, 'r') as fo:
                    temp2 = re.findall(regex_str, fo.read())
                for i in range(len(temp2)):
                    broken_NK_file.write(temp2[i]+'\n')
                
                print(f'{alpha+1}/{my_alpha} - {Z+1}/{my_Z} - {counter+1}/8 - {len(temp2)}')
            broken_NK_file.close()

print('='*70)